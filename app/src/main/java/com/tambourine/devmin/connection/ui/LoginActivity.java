package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.kakao.auth.KakaoSDK;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends MinCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public static final int FIND_PASS_RETURN = 1111;
    public static final String TAG = "ConnectionAuthTAG";

    EditText edtEmail, edtPass;
    LinearLayout layInputEmail, layInputPass, layEmail, layJoin, layFindPass, layFacebook, layGoogle, layKakao;

    private static final int RC_SIGN_IN = 9001;
    public static final int REQUEST_CODE_KAKAO_LOGIN = 1000;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;

    private CallbackManager mCallbackManager;
    LoginManager loginManager;
    List<String> permissionNeeds = new ArrayList<>();
    String id = "", birth = "", gender = "", name = "", pw = "", profile_url = "";
    String tempId, tempPw, tempType, tempNick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = (EditText) findViewById(R.id.edt_login_email);
        edtPass = (EditText) findViewById(R.id.edt_login_pass);
        layInputEmail = (LinearLayout) findViewById(R.id.lay_login_input_email);
        layInputPass = (LinearLayout) findViewById(R.id.lay_login_input_pass);
        layEmail = (LinearLayout) findViewById(R.id.lay_login_email);
        layJoin = (LinearLayout) findViewById(R.id.lay_login_join);
        layFindPass = (LinearLayout) findViewById(R.id.lay_login_find_pass);
        layFacebook = (LinearLayout) findViewById(R.id.lay_login_facebook);
        layGoogle = (LinearLayout) findViewById(R.id.lay_login_google);
        layKakao = (LinearLayout) findViewById(R.id.lay_login_kakao);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();

        mAuth.signOut();
        // [END initialize_auth]

        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Map<String, String> parmas = new HashMap<String, String>();
                    parmas.put("id", user.getUid());

                    tempId = user.getEmail();
                    if (tempId == null) {
                        tempId = user.getUid();
                    }
                    tempPw = user.getUid();
                    tempType = "10003";
                    tempNick = user.getDisplayName();

                    new APITaskNew(LoginActivity.this, parmas, ConnectionController.API_CHECK_JOIN, stmCheck).execute();
                    Log.e(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.e(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]
                updateUI(user);
                // [END_EXCLUDE]
            }
        };

        mCallbackManager = CallbackManager.Factory.create();
        try {
            KakaoSDK.init(new LoginKakaoActivity.KakaoSDKAdapter());
        } catch (KakaoSDK.AlreadyInitializedException e) {

        }

        permissionNeeds.add("email");
        permissionNeeds.add("user_birthday");
        permissionNeeds.add("public_profile");

        FacebookSdk.sdkInitialize(getApplicationContext());
        loginManager = LoginManager.getInstance();

        loginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                final GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                if (object == null) {

                                } else {
                                    Log.e("aaa", object.toString());
                                    try {
                                        if (object.isNull("email")) {
                                            id = object.getString("id");
                                        } else {
                                            id = object.getString("email");
                                        }
                                        if (object.isNull("birthday")) {
                                            birth = "01/01/1900";
                                        } else {
                                            birth = object.getString("birthday");
                                        }
                                        gender = object.getString("gender");
                                        name = object.getString("name");
                                        pw = object.getString("id");
                                        JSONObject j = new JSONObject(object.getString("picture"));
                                        JSONObject jj = new JSONObject(j.getString("data"));
                                        profile_url = jj.getString("url");

                                        Log.e("data", id + "/" + birth + "/" + gender + "/" + name + "/" + pw + "/" + profile_url);

                                        String[] births = birth.split("/");
                                        String realBirth = "";
                                        realBirth = births[2] + "/" + births[0] + "/" + births[1];

                                        Log.e("realBirth", realBirth);

                                        boolean realGender = true;

                                        realGender = gender.equals("male");

                                        JSONObject j1 = new JSONObject(object.toString());

//                                        JSONArray jarr = new JSONArray(j2.getString("data"));


                                        final String finalRealBirth = realBirth;
                                        final boolean finalRealGender = realGender;

                                        //TODO 회원가입 확인 및 로그인

                                        Map<String, String> parmas = new HashMap<String, String>();
                                        parmas.put("id", id);
                                        parmas.put("type", "10002");

                                        tempId = id;
                                        tempPw = pw;
                                        tempType = "10002";
                                        tempNick = name;

                                        new APITaskNew(LoginActivity.this, parmas, ConnectionController.API_CHECK_JOIN, stmCheck).execute();

//                                        join(id, pw, name, "10002");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture,friends");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        layEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectionController.se.putInt("loginType", 0);
                ConnectionController.se.commit();
                login();
            }
        });
        layJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, Join1Activity.class));
                overridePendingTransition(0, 0);
                finish();
            }
        });
        layFindPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(LoginActivity.this, FindPassActivity.class), FIND_PASS_RETURN);
                overridePendingTransition(0, 0);
            }
        });
        layFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 페이스북 로그인
//                Toast.makeText(getApplicationContext(), "facebook", Toast.LENGTH_SHORT).show();
//                ConnectionController.se.putInt("loginType", 1);
//                ConnectionController.se.commit();
//                login();
                loginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "user_birthday", "public_profile"));
            }
        });
        layGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 구글+ 로그인
//                Toast.makeText(getApplicationContext(), "google", Toast.LENGTH_SHORT).show();
//                ConnectionController.se.putInt("loginType", 2);
//                ConnectionController.se.commit();
//                login();
                signIn();
            }
        });
        layKakao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 카카오 로그인
//                Toast.makeText(getApplicationContext(), "kakao", Toast.LENGTH_SHORT).show();
//                ConnectionController.se.putInt("loginType", 3);
//                ConnectionController.se.commit();
//                login();
                startKakaoLogin();
            }
        });

    }

    public void startKakaoLogin() {
        Intent intent = new Intent(LoginActivity.this, LoginKakaoActivity.class);
        startActivityForResult(intent, REQUEST_CODE_KAKAO_LOGIN);

    }

    private void login() {

        if (edtEmail.getText().toString().length() > 0) {
            if (edtPass.getText().toString().length() > 0) {

                tempPw = edtPass.getText().toString();

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", edtEmail.getText().toString());
                params.put("pw", edtPass.getText().toString());
                params.put("gcm", ConnectionController.PUSH_TOKEN);
                params.put("type", "10001");

                new APITaskNew(LoginActivity.this, params, ConnectionController.API_LOGIN, loginResult).execute();
            } else {
                layInputPass.startAnimation(ConnectionController.shake);
            }
        } else {
            layInputEmail.startAnimation(ConnectionController.shake);
        }
    }

    private StringTransMethod loginResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                ConnectionController.se.putBoolean("isLogin", true);
                ConnectionController.se.putInt("mIdx", Integer.parseInt(jd.getString("mIdx")));
                ConnectionController.se.putString("mId", jd.getString("mId"));
                ConnectionController.se.putString("mPw", tempPw);
                ConnectionController.se.putString("mName", jd.getString("mName"));
                ConnectionController.se.putInt("mLoc", Integer.parseInt(jd.getString("mLoc")));
                ConnectionController.se.putInt("mBirth", Integer.parseInt(jd.getString("mBirth")));
                ConnectionController.se.putInt("mSex", Integer.parseInt(jd.getString("mSex")));
                ConnectionController.se.putBoolean("mConfirm", Integer.parseInt(jd.getString("mConfirm")) == 0 ? false : true);
                ConnectionController.se.putBoolean("mBlock", Integer.parseInt(jd.getString("mBlock")) == 0 ? false : true);
                ConnectionController.se.putString("mBlockDate", jd.getString("mBlockDate"));
                ConnectionController.se.putString("mBlockReason", jd.getString("mBlockReason"));
                ConnectionController.se.putInt("mAntenna", Integer.parseInt(jd.getString("mAntenna")));
                ConnectionController.se.putInt("mJoinType", Integer.parseInt(jd.getString("mJoinType")));
                ConnectionController.se.putString("mLoveType", jd.getString("mLoveType"));
                ConnectionController.se.putString("mRegDate", jd.getString("mRegDate"));
                ConnectionController.se.commit();

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.fade, R.anim.hold);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FIND_PASS_RETURN:
                if (resultCode == RESULT_OK) {
                    edtEmail.setText(data.getExtras().getString("email"));
                    Toast.makeText(getApplicationContext(), "비밀번호 변경완료 이메일을 확인해주세요.", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_CODE_KAKAO_LOGIN:

                if (data != null && resultCode == RESULT_OK) {

                    String accessToken = data.getStringExtra(LoginKakaoActivity.INTENT_EXTRA_ACCESS_TOKEN);
                    Log.e("accToken", accessToken);
//                    tv_access_token.setText(accessToken);

                    UserManagement.requestMe(new MeResponseCallback() {
                        @Override
                        public void onSessionClosed(ErrorResult errorResult) {

                        }

                        @Override
                        public void onNotSignedUp() {

                        }

                        @Override
                        public void onSuccess(UserProfile result) {
                            Log.e("result", result.toString());
                            Log.e("rr", result.getId() + "");
                            Log.e("rr2", result.getUUID() + "");
//                            Log.e("result", result.getUUID());
//                            Log.e("result", result.getId() + "");

                            //TODO 회원가입확인 및 로그인
                            tempId = result.getId() + "";
                            tempPw = result.getId() + "";
                            tempType = "10004";
                            tempNick = result.getNickname() + "";

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("id", tempId);

                            new APITaskNew(LoginActivity.this, params, ConnectionController.API_CHECK_JOIN, stmCheck).execute();

//                            join(result.getId() + "", result.getId() + "", result.getNickname() + "", "10003");
                        }
                    });

                } else {

                    Toast.makeText(this, "토큰 가져오기 에러", Toast.LENGTH_SHORT).show();
                }


                break;
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
    }

    private StringTransMethod stmCheck = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getString("result").equals("0")) {
                    Log.e("check ok", tempId + "/" + tempPw + "/" + tempNick + "/" + tempType);
                    Intent i = new Intent(LoginActivity.this, Join2Activity.class);
                    i.putExtra("email", tempId);
                    i.putExtra("pass", tempPw);
                    i.putExtra("nick", tempNick);
                    i.putExtra("type", tempType);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (jd.getString("result").equals("1")) {
                    //TODO 로그인
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("id", tempId);
                    params.put("pw", tempPw);
                    params.put("gcm", ConnectionController.PUSH_TOKEN);
                    params.put("type", tempType);

                    new APITaskNew(LoginActivity.this, params, ConnectionController.API_LOGIN, loginResult).execute();
                } else {
                    ConnectionController.getToast(getApplicationContext(), getResources().getString(R.string.warning_api_error));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
