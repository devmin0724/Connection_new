package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.ChattingListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.ChattingDetailListItem;
import com.tambourine.devmin.connection.vo.ConnectionListItem;
import com.tambourine.devmin.connection.widget.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class ChattingListActivity extends MinCompatActivity {
    RecyclerViewEmptySupport rv;

    FastItemAdapter<ChattingListUIItem> adapter;
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_list);

        rv = (RecyclerViewEmptySupport) findViewById(R.id.rv_chatting_list);

        adapter = new FastItemAdapter<>();

        rv.setLayoutManager(new LinearLayoutManager(this));

        rv.setEmptyView(findViewById(R.id.tv_chatting_list_empty_content));

        rv.setAdapter(adapter);

        loadData();
    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("page", page + "");

        new APITaskNew(this, params, ConnectionController.API_GET_CHATTING_LIST, clResult).execute();

    }

    private StringTransMethod clResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);

                Calendar _c = Calendar.getInstance();
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    ChattingDetailListItem item = new ChattingDetailListItem();
                    item.setCcId(Integer.parseInt(jd.getString("ccId")));
                    item.setCcSendeer(Integer.parseInt(jd.getString("ccSender")));
                    item.setCcTarget(Integer.parseInt(jd.getString("ccTarget")));
                    item.setCcIng(Integer.parseInt(jd.getString("ccIng")));
                    item.setCcLastStr(jd.getString("ccLastStr"));
                    item.setCcSenderView(jd.getString("ccSenderView").equals("1") ? true : false);
                    item.setCcTargetView(jd.getString("ccTargetView").equals("1") ? true : false);
                    item.setCcAntenna(Integer.parseInt(jd.getString("ccAntenna")));
                    item.setCcRegDate(jd.getString("ccRegDate"));

                    item.setSex(Integer.parseInt(jd.getString("mSex")));
                    item.setBirth(_c.get(Calendar.YEAR) - Integer.parseInt(jd.getString("mBirth")) + 1);
                    item.setLoc(Integer.parseInt(jd.getString("mLoc")));

                    adapter.add(new ChattingListUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };
}