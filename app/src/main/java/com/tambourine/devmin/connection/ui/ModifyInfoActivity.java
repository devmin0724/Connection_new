package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.popup.LocationPopupActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidwheelview.dusunboy.github.com.library.dialog.OnTextSetListener;
import androidwheelview.dusunboy.github.com.library.dialog.WheelViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ModifyInfoActivity extends MinCompatActivity {

    @BindView(R.id.edt_modify_info_name)
    EditText edtName;
    @BindView(R.id.edt_modify_info_pass1)
    EditText edtPass1;
    @BindView(R.id.edt_modify_info_pass2)
    EditText edtPass2;
    @BindView(R.id.edt_modify_info_pass3)
    EditText edtPass3;
    @BindView(R.id.tv_modify_info_location)
    TextView tvLocation;

    @BindView(R.id.lay_modify_info_name)
    LinearLayout layName;
    @BindView(R.id.lay_modify_info_pass_1)
    LinearLayout layPass1;
    @BindView(R.id.lay_modify_info_pass_2)
    LinearLayout layPass2;
    @BindView(R.id.lay_modify_info_pass_3)
    LinearLayout layPass3;
    @BindView(R.id.lay_modify_info_location)
    LinearLayout layLocation;

    @BindView(R.id.lay_modify_info_ok)
    LinearLayout layOk;

    String location;
    int locationNum = -1;
    WheelViewDialog locationDialog;
    boolean isPassPatternMatch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_info);
        ButterKnife.bind(this);

        edtName.setText(ConnectionController.sp.getString("mName", ""));
        locationNum = ConnectionController.sp.getInt("mLoc", 0);
        tvLocation.setText(getResources().getStringArray(R.array.location)[locationNum]);

        locationDialog = new WheelViewDialog(ModifyInfoActivity.this, WheelViewDialog.ONE_LEVEL);
        locationDialog.setData(ConnectionController.locations);
        locationDialog.setPositiveButton("확인", new OnTextSetListener() {
            @Override
            public void onTextSet(WheelViewDialog wheelViewDialog, String text) {
                wheelViewDialog.dismiss();
                location = text;

                for (int i = 0; i < getResources().getStringArray(R.array.location).length; i++) {
                    if (getResources().getStringArray(R.array.location)[i].equals(text)) {
                        locationNum = i;
                    }
                }

                tvLocation.setText(location);
            }
        });
        locationDialog.setTitle("지역 선택");

        edtPass2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String temp = charSequence.toString();
                if (temp.length() > 0) {
                    isPassPatternMatch = ConnectionController.checkPass(temp);
                } else {
                    isPassPatternMatch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        layLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(ModifyInfoActivity.this, LocationPopupActivity.class), 1111);
                overridePendingTransition(0, 0);
            }
        });

        layOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInfo();
//                finish();
//                overridePendingTransition(0, 0);
            }
        });
    }

    private void checkInfo() {
        if (edtName.getText().toString().length() > 0) {
            if (edtPass1.getText().toString().length() > 0) {
                if (edtPass2.getText().toString().length() > 0) {
                    if (isPassPatternMatch) {
                        if (edtPass2.getText().toString().equals(edtPass3.getText().toString())) {
                            if (locationNum > -1) {
                                HashMap<String, String> parmas = new HashMap<>();
                                parmas.put("id", ConnectionController.sp.getString("mId", ""));
                                parmas.put("name", edtName.getText().toString());
                                parmas.put("pw", edtPass1.getText().toString());
                                parmas.put("newPw", edtPass2.getText().toString());
                                parmas.put("loc", locationNum + "");

                                new APITaskNew(ModifyInfoActivity.this, parmas, ConnectionController.API_MODIFY_INFO, miResult).execute();
                            } else {
                                layLocation.startAnimation(ConnectionController.shake);
                                getToast("지역을 선택해주세요.");
                            }
                        } else {
                            layPass2.startAnimation(ConnectionController.shake);
                            layPass3.startAnimation(ConnectionController.shake);
                            getToast("비밀번호가 일치하지 않습니다.");
                        }
                    } else {
                        layPass2.startAnimation(ConnectionController.shake);
                        getToast("비밀번호 형식을 확인해주세요. 영어 숫자 조합 6~15자리");
                    }
                } else {
                    layPass2.startAnimation(ConnectionController.shake);
                    getToast("새로운 비밀번호를 입력해주세요.");
                }
            } else {
                layPass1.startAnimation(ConnectionController.shake);
                getToast("현재 비밀번호를 입력해주세요.");
            }
        } else {
            layName.startAnimation(ConnectionController.shake);
            getToast("이름을 입력해주세요.");
        }
    }

    private StringTransMethod miResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getString("result").equals("0")) {
                    getToast("정보수정이 완료되었습니다.");
                    ConnectionController.se.putString("mPw", edtPass2.getText().toString());
                    ConnectionController.se.putInt("mLoc", locationNum);
                    ConnectionController.se.putString("mName", edtName.getText().toString());
                    ConnectionController.se.commit();
                    finish();
                    overridePendingTransition(0, 0);
                } else if (jd.getString("result").equals("2")) {
                    layPass1.startAnimation(ConnectionController.shake);
                    getToast("비밀번호를 확인해주세요.");
                    edtPass1.setText("");
                } else {
                    ConnectionController.getToast(getApplicationContext(), getResources().getString(R.string.warning_api_error));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void getToast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1111:
                if (resultCode == RESULT_OK) {
                    location = data.getExtras().getString("result");
                    locationNum = data.getExtras().getInt("resultNum");
                    tvLocation.setText(location);
                }
                break;
        }
    }
}
