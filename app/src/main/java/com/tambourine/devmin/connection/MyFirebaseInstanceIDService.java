package com.tambourine.devmin.connection;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tambourine.devmin.connection.utils.APITaskNonActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.utils.ConnectionController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by devmin on 2016-09-09.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        ConnectionController.PUSH_TOKEN = refreshedToken;

        //Displaying token on logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
//        sendRegistrationToServer();
    }

    StringTransMethod loginResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            Log.e(TAG, "Refreshed token: " + ConnectionController.PUSH_TOKEN);
        }
    };

    private void sendRegistrationToServer() {
        //You can implement this method to store the token on your server
        //Not required for current project

        Map<String, String> params = new HashMap<>();
        params.put("GCM", ConnectionController.PUSH_TOKEN);
        params.put("DID", ConnectionController.DID);

        new APITaskNonActivity(this, params, ConnectionController.API_GCM, loginResult).execute();
    }
}