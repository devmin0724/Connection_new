package com.tambourine.devmin.connection.items;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.DateActivity;
import com.tambourine.devmin.connection.ui.popup.ConnectionDetailPopupActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.ConnectionDetailItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class ConnectionDetailUIItem extends AbstractItem<ConnectionDetailUIItem, ConnectionDetailUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public ConnectionDetailItem item;
    private AlertDialog.Builder adb;
    private AlertDialog ad;

    public ConnectionDetailUIItem withItem(ConnectionDetailItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.connection_detail_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_connection_detail;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(final ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        final StringTransMethod cancelResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                setType(con, holder, 5);
            }
        };
        final StringTransMethod rejectResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                setType(con, holder, 4);
            }
        };
        final StringTransMethod accessResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    if (jd.getString("result").equals("4")) {
                        Toast.makeText(con.getApplicationContext(), "안테나가 부족합니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        setType(con, holder, 2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        holder.tvDate.setText(item.getDate());
        switch (item.getNowStatus()) {
            case 1:
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_whitelove);
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.tvBtn.setText("데이트 취소");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 취소하면 " + item.getAntennaCount() + "개의 안테나가 반환됩니다.");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "1");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, cancelResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });

                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 취소하면 " + item.getAntennaCount() + "개의 안테나가 반환됩니다.");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "1");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, cancelResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                } else {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.VISIBLE);
                    holder.tvBtn.setText("수락");
                    holder.tvBtn2.setText("거절");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 수락하면 " + item.getAntennaCount() + "개의 안테나가 차감됩니다.");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "2");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, accessResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 거절하시겠습니까?");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "0");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, rejectResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO 아무것도안함
                        }
                    });
                }
                holder.tvMent.setText(item.getDateName() + " 대기중");

                break;
            case 2:

                if ((ConnectionController.sp.getInt("mSex", 0) == 1 && item.isMComplete())
                        || (ConnectionController.sp.getInt("mSex", 0) == 0 && item.isWComplete())) {
                    holder.layBtn.setVisibility(View.GONE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.imvIcon.setImageResource(R.drawable.icon_notice_pinklove);
                    holder.tvBtn.setText("");
                    holder.tvMent.setText(item.getDateName() + " 상대방 진행중");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                } else {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.imvIcon.setImageResource(R.drawable.icon_notice_pinklove);
                    holder.tvBtn.setText("계속하기");
                    holder.tvMent.setText(item.getDateName() + " 진행중");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(con, DateActivity.class);
                            i.putExtra("id", item.getId());
//                        i.putExtra("imgName", item.get);
                            i.putExtra("type", item.getType());
                            con.startActivity(i);
                            ((Activity) con).overridePendingTransition(0, 0);
                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(con, DateActivity.class);
                            i.putExtra("id", item.getId());
                            i.putExtra("type", item.getType());
                            con.startActivity(i);
                            ((Activity) con).overridePendingTransition(0, 0);
                        }
                    });
                }

                //TODO 대기중 if 검사한후 추가해야함

                break;
            case 3:
                holder.layBtn.setVisibility(View.VISIBLE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowlove);
                holder.tvBtn.setText("상세보기");
                holder.tvMent.setText(item.getDateName() + " 완료");
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(con, ConnectionDetailPopupActivity.class);
                        i.putExtra("id", item.getId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(con, ConnectionDetailPopupActivity.class);
                        i.putExtra("id", item.getId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                break;
            case 4:
                holder.layBtn.setVisibility(View.GONE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_graylove);
                holder.tvBtn.setText("계속하기");
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.tvMent.setText(item.getDateName() + " 거절됨");
                } else {
                    holder.tvMent.setText(item.getDateName() + " 거절함");
                }
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                break;
            case 5:
                holder.layBtn.setVisibility(View.GONE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_graylove);
                holder.tvBtn.setText("계속하기");
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.tvMent.setText(item.getDateName() + " 취소");
                } else {
                    holder.tvMent.setText(item.getDateName() + " 취소");
                }
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                break;
        }

//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(con, ConnectionDetailPopupActivity.class);
////                        i.putExtra("id", id);
//                con.startActivity(i);
//                ((Activity) con).overridePendingTransition(0, 0);
//            }
//        });
    }

    private void setType(final Context con, final ViewHolder holder, int type) {
        final StringTransMethod cancelResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                setType(con, holder, 5);
            }
        };
        final StringTransMethod rejectResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                setType(con, holder, 4);
            }
        };
        final StringTransMethod accessResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    if (jd.getString("result").equals("4")) {
                        Toast.makeText(con.getApplicationContext(), "안테나가 부족합니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        setType(con, holder, 2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        switch (type) {
            case 1:
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_whitelove);
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.tvBtn.setText("데이트 취소");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 거절하시겠습니까?");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "1");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, cancelResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });

                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 거절하시겠습니까?");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "1");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, cancelResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                } else {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.VISIBLE);
                    holder.tvBtn.setText("수락");
                    holder.tvBtn2.setText("거절");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 거절하시겠습니까?");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "2");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, accessResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder buider = new AlertDialog.Builder(con);

                            View v = (View) (((Activity) con).getLayoutInflater().
                                    inflate(R.layout.popup_date, null));

                            TextView tvBody = (TextView) v.findViewById(R.id.tv_popup_date_body);
                            tvBody.setText("데이트를 거절하시겠습니까?");

                            LinearLayout layYes = (LinearLayout) v.findViewById(R.id.lay_popup_date_yes);
                            LinearLayout layNo = (LinearLayout) v.findViewById(R.id.lay_popup_date_no);

                            layYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO yes
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
                                    params.put("dclId", item.getId() + "");
                                    params.put("access", "0");

                                    new APITaskNew(con, params, ConnectionController.API_DATE_ACCESS, rejectResult).execute();
                                    ad.dismiss();
                                }
                            });

                            layNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO no
                                    ad.dismiss();
                                }
                            });

                            buider.setView(v);

                            ad = buider.create();
                            ad.show();
                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO 아무것도안함
                        }
                    });
                }
                holder.tvMent.setText(item.getDateName() + " 대기중");

                break;
            case 2:
                if ((ConnectionController.sp.getInt("mSex", 0) == 1 && item.isMComplete())
                        || (ConnectionController.sp.getInt("mSex", 0) == 0 && item.isWComplete())) {
                    holder.layBtn.setVisibility(View.GONE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.imvIcon.setImageResource(R.drawable.icon_notice_pinklove);
                    holder.tvBtn.setText("");
                    holder.tvMent.setText(item.getDateName() + " 상대방 진행중");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                } else {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.imvIcon.setImageResource(R.drawable.icon_notice_pinklove);
                    holder.tvBtn.setText("계속하기");
                    holder.tvMent.setText(item.getDateName() + " 진행중");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(con, DateActivity.class);
                            i.putExtra("id", item.getId());
//                        i.putExtra("imgName", item.get);
                            i.putExtra("type", item.getType());
                            con.startActivity(i);
                            ((Activity) con).overridePendingTransition(0, 0);
                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(con, DateActivity.class);
                            i.putExtra("id", item.getId());
                            i.putExtra("type", item.getType());
                            con.startActivity(i);
                            ((Activity) con).overridePendingTransition(0, 0);
                        }
                    });
                }
                break;
            case 3:
                holder.layBtn.setVisibility(View.VISIBLE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowlove);
                holder.tvBtn.setText("상세보기");
                holder.tvMent.setText(item.getDateName() + " 완료");
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(con, ConnectionDetailPopupActivity.class);
                        i.putExtra("id", item.getId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(con, ConnectionDetailPopupActivity.class);
                        i.putExtra("id", item.getId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                break;
            case 4:
                holder.layBtn.setVisibility(View.GONE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_graylove);
                holder.tvBtn.setText("계속하기");
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.tvMent.setText(item.getDateName() + " 거절됨");
                } else {
                    holder.tvMent.setText(item.getDateName() + " 거절함");
                }
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                break;
            case 5:
                holder.layBtn.setVisibility(View.GONE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_graylove);
                holder.tvBtn.setText("계속하기");
                if (Integer.parseInt(item.getSenderId()) == ConnectionController.sp.getInt("mIdx", 1)) {
                    holder.tvMent.setText(item.getDateName() + " 취소");
                } else {
                    holder.tvMent.setText(item.getDateName() + " 취소");
                }
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 아무것도안함
                    }
                });
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.imv_row_connection_detail_icon)
        protected ImageView imvIcon;
        @BindView(R.id.tv_row_connection_detail_date)
        protected TextView tvDate;
        @BindView(R.id.tv_row_connection_detail_ment)
        protected TextView tvMent;
        @BindView(R.id.tv_row_connection_detail_btn)
        protected TextView tvBtn;
        @BindView(R.id.tv_row_connection_detail_btn2)
        protected TextView tvBtn2;
        @BindView(R.id.lay_row_connection_detail_btn)
        protected LinearLayout layBtn;
        @BindView(R.id.lay_row_connection_detail_btn2)
        protected LinearLayout layBtn2;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
