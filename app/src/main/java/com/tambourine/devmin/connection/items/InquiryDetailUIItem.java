package com.tambourine.devmin.connection.items;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.InquiryDetailItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class InquiryDetailUIItem extends AbstractItem<InquiryDetailUIItem, InquiryDetailUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public InquiryDetailItem item;

    public InquiryDetailUIItem withItem(InquiryDetailItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.inquiry_detail_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_inquiry_detail;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        if (item.isMe()) {
            holder.tv1.setText("[나] : ");
            holder.tv2.setText(item.getMent());
            holder.tv1.setTextColor(0xffaaeaf4);
            holder.tv2.setTextColor(0xffaaeaf4);
        } else {
            holder.tv1.setText("[상대] : ");
            holder.tv2.setText(item.getMent());
            holder.tv1.setTextColor(0xffffffff);
            holder.tv2.setTextColor(0xffffffff);
        }

//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //TODO 문의 상세로 이동
//                con.startActivity(new Intent(con, InquiryDetailActivity.class));
//                ((Activity) con).overridePendingTransition(0, 0);
//                //item.getId();
//            }
//        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.tv_row_inquiry_detail_1)
        protected TextView tv1;
        @BindView(R.id.tv_row_inquiry_detail_2)
        protected TextView tv2;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
