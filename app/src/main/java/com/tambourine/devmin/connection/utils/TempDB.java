package com.tambourine.devmin.connection.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by devmin on 2016-12-08.
 */

public class TempDB {
    public static ArrayList<String> questionsW1 = new ArrayList<String>(Arrays.asList(
            "이틀 전, 지인의 소개로 이루어진 첫 소개팅 이후 간간히 sns로 연락만을 주고 받던 남자에게서 내일 저녁, 영화를 보자고 카톡이 왔다. 완전한 이상형은 아니지만 그렇게 마음에 들지도 않는 사이. 한번쯤은 에프터에 응할 마음이 있는데.. 남자의 제안에 대한 당신의 답변은?",
            "영화를 보기로 한 당일. 소개팅 이후, 첫만남이라 그런지 괜스레 더 긴장되고 떨리는데... 불타는 금요일 야심한 저녁 8시. 잘될지 모르는 남자와의 데이트자리, 당신이 입을 옷차림은?",
            "약속을 잡아 만난 남자와 여자. 한껏 꾸민 서로의 모습과 달리 둘 사이의 공기에는 긴장감과 어색함이 역력한데... 티켓을 구입하기 위해 창구를 향하는 남자와 여자. 먼저 영화를 고르라고 말하는 남자. 당신의 선택은?",
            "함께 볼 영화는 선택됐지만 또 다른 관문이 남았다. 남자와 처음 보는 영화, 당신은 어디 앉을 것인가?",
            "영화값을 계산하겠다며 지갑을 꺼내든 남자. 그런데 이게 왠일? 현금은 없고 카드는 정지상태다. 매우 민망한 상황에 남자는 어찌할 바를 몰라하고 직원은 커플을 쳐다보고 있는데.. 이 때 당신의 행동은?",
            "회사에서 간단하게 요기를 한 상태이지만 무언가 달달한게 땡기는 이 시기. 영화 보기엔 아직 시간이 30분 정도 남았는데... 이때 당신이 향하는 곳은?",
            "우여곡절 끝에 영화관에 자리잡은 당신과 남자. 한창 영화에 집중할 시기, 갑자기 회사 상사로 부터 전화가 온다. 스트레스 작렬의 노처녀 대리다. 전화를 받기 위해선 남자를 지나 멀고 먼 길을 거쳐야지만 되는데... 이때 당신의 선택은?",
            "영화는 끝이 났다. 엔딩크레딧이 올라갈 시기. 당신의 행동은?",
            "재미있을 것 같아 내가 선택했지만.. 예상과는 달리 영화는 너무 재미가 없었는데.. 막상 영화관을 나오니 할 말이 딱히 없다. 영화를 봤으니 얘기는 해야겠고 이때 당신이 남자에게 처음 건넬 말은 무엇인가?",
            "어언 시간은 열한시를 향하고. 그때 남자가 2차를 제안하는데.. 내일이 토요일이지만 출근을 해야하는 당신.  지금까지의 대화는 나쁘지 않았다! 이 시각, 당신은 어디로 떠날 것인가?"
    ));
    public static ArrayList<String> questionsM1 = new ArrayList<String>(Arrays.asList(
            "이틀 전, 지인의 소개로 이루어진 첫 소개팅 이후 간간히 sns로 연락만을 주고 받던 여자에게 내일 저녁, 영화를 보자고 제안하고자 한다. 호감 있는 여자에게 건네는 첫 데이트 신청! 이때 당신이 취하는 행동은 무엇인가?",
            "영화를 보기로 한 당일. 소개팅 이후, 첫만남이라 그런지 괜스레 더 긴장되고 떨리는데... 불타는 금요일 야심한 저녁 8시. 잘될지 모르는 남자와의 데이트자리, 당신이 입을 옷차림은?",
            "약속을 잡아 만난 남자와 여자. 한껏 꾸민 서로의 모습과 달리 둘 사이의 공기에는 긴장감과 어색함이 역려한데... 티켓을 구입하기 위해 창구를 향하는 남녀. 이때 당신이 취할 행동은?",
            "함께 볼 영화는 선택됐지만 또 다른 관문이 남았다. 여자와 처음 보는 영화, 당신은 어디 앉을 것인가?",
            "영화값을 계산하겠다고 지갑을 꺼낸 당신. 그런데 이게 왠일? 현금은 없고 카드는 정지상태다. 매우 민망한 이 상황에, 직원은 멀뚱히 당신을 쳐다보고 있고. 여자는 아무것도 모른 채 당신의 행동만을 기다리고 있다. 이 때 당신이 취할 행동은 무엇인가?",
            "회사에서 간단하게 요기를 한 상태이지만 무언가 달달하게 땡기는 이 시기. 영화 보기엔 아직 시간이 30분 정도 남았는데... 이때 당신이 향하는 곳은?",
            "우여곡절 끝에 영화관에 자리잡은 여자와 당신. 한창 영화에 집중할 시기, 갑자기 여자의 핸드폰에서 진동이 울리는데.. 아직 여자는 눈치채지 못한 상태. 이때 당신이 취할 행동은?",
            "영화는 끝이 났다. 엔딩크레딧이 올라갈 시기. 당신의 행동은?",
            "여자가 선택한 영화이건만, 영화는 너무 재미가 없었다. 막상 영화관을 나오니 할 말이 딱히 없고.. 영화를 봤으니 얘기는 해야겠고 이때 당신이 여자에게 처음 건넬 말은 무엇인가?",
            "어언 시간은 열한시를 향하고. 그냥 여자를 보내기엔 뭔가 아쉬운 상태. 때마침 내일은 토요일이다. 지금까지의 데이트는 나쁘지 않았다! 이 시각 당신의 행동은?"
    ));

    public static ArrayList<ArrayList<String>> aW1 = new ArrayList<ArrayList<String>>(Arrays.asList(
            new ArrayList<String>(Arrays.asList(
                    "제안한 남자가 민망하지 않게 곧장 \"좋아요 ^0^\" 등의 우호적인 답변을 보낸다.",
                    "쉬운 여자로 보이기 싫은 당신, 일단 한번쯤은 \"글쎄요, 잠시만요. 스케줄 확인 좀\" 등의 대답을 한다.",
                    "남자의 말만을 기다렸다는 듯 바로 대답하기엔 자존심이 상한다. 한시간 즈음 뒤에 애매모호한 답변을 보낸다.",
                    "\"어떤 영화요?\" 무슨 영화를 볼지에 따라 대답은 달라진다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "여자라면 당연히, 여성스러운 꽃무늬 원피스. 더불어 샤랄라한 레이스는 덤이다.",
                    "차도녀의 표본. 얼핏보면 섹시하게 느껴지는 오피스룩으로 남심을 사로잡겠다.",
                    "영화관에선 편한게 최고지! 청바지에 후드티가 제격",
                    "데이트 복장의 표본을 깨버리겠다. 호피무늬가 들어간 셔츠에 라이더자켓."
            )),
            new ArrayList<String>(Arrays.asList(
                    "남자의 의견은 묻지 않은 채, 평소 자신이 보고 싶었던 영화를 고른다.",
                    "어색한 사이에서는 역시 한참 흥행하는 블록버스터 영화지~",
                    "수준 있는 사람으로 보이고 싶다. 한창 외국영화제에서 상을 휩쓴 예술 영화",
                    "데이트엔 당연히 로코물 아니야? 국내 톱스타가 나오는, 가벼운 로맨틱코미디 영화"
            )),
            new ArrayList<String>(Arrays.asList(
                    "에잇, 이번 기회에 더욱 친해지자! 커플석",
                    "혹시 모를 스킨쉽을 대비해 맨 뒷자리 사이드석",
                    "일단 영화부터 잘 보는게 제일 중요하지! 정중앙자리",
                    "자리가 뭣이 중한디? 아무데나!"
            )),
            new ArrayList<String>(Arrays.asList(
                    "민망해 자신의 돈으로 황급히 계산을 하고 자리를 옮긴다.",
                    "직원 앞에서 남자가 민망하지 않게 \"아, 자기야. 아까 자기 카드 나한테 줬잖아\" 라고 말한 뒤 자신의 카드로 여유롭게 계산한다.",
                    "이런 기분으로 뭔 영화야! 남자를 데리고 일단 그 자리에서 벗어난다.",
                    "영화보자고 말하면서 금전상태도 확인하지 않고 와? 준비태도가 실망스럽다. 남자를 두고 나홀로 자리를 옮긴다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "지금 시간이 몇시인데? 야식은 금물! 그냥 영화를 기다리며 팜플렛을 본다.",
                    "영화엔 팝콘과 콜라가 진리지. 더불어 핫도그, 나초도!",
                    "당연히 커피 아니야? 남자와 근처 카페에 들어가 커피를 주문한다.",
                    "별로 땡기지도 않고.. 그렇다고 안 땡기는 것도 아니고... 상대방의 의견에 따른다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "퇴근 후면 당연히 업무 종료지. 못 본 척 핸드폰을 가방 깊숙히 밀어넣는다.",
                    "내일의 후폭풍이 두렵다. 남자에게 양해를 구하고 밖으로 나간다.",
                    "전화는 받지 않은 채 바로 문자를 날린다.",
                    "어찌할 바를 몰라 남자의 눈치를 보다 \"어떻게 할까요?\" 질문한다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "영화는 엔딩크레딧까지지! 영화제작에 참여한 모든 이들의 이름이 나올때까지 가만히 앉아 스크린을 본다.",
                    "이름이 뭣이 중한디! 영화가 끝나자마자 바로 일어나 나갈 준비를 한다.",
                    "어쩌지? 다른 좌석 사람들의 행렬에 자연스럽게 일어나 합류한다.",
                    "조용히 남자의 눈치를 살피며 기다린다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "솔직한게 제일이다. 먼저 선수치자! '너무 재미 없었죠? 헤헤. 재밌을 줄 알았는데...'",
                    "데이트에 부정적인 말은 질색이다! 어떻게든 포장하자. \"배우가... 연기는 좋더라구요!\"",
                    "나만 재미없다 느꼈을 수 있다. 일단 상대방의 반응을 파악하자. \"영화.. 어떠셨어요?\"",
                    "에잇, 머리아픈 건 딱 질색. 상대가 먼저 말하기 까지 기다린다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "지금 시간이 몇시야! 공과사는 구분하자. 일이 먼저다. 정중히 양해를 구하고 집에 간다.",
                    "이렇게 제안하기 까지, 얼마나 남자도 고민이 많았을까? 내키진 않지만 남자의 제안에 응한 뒤, 근처 카페로 간다.",
                    "드라이브하면서 이야기를 하자고 하고. 집까지 데려다 달라고 한 뒤, 그 시간을 이용해 이야기를 나눈다.",
                    "이제 좀 친해져보자! 화끈하게 근처 맥주집으로 향한다."
            ))
    ));
    public static ArrayList<ArrayList<String>> aM1 = new ArrayList<ArrayList<String>>(Arrays.asList(
            new ArrayList<String>(Arrays.asList(
                    "바로 통화버튼을 눌러 직접 만나자고 이야기한다.",
                    "어색하지 않게 톡으로 각종 이모티콘을 덧붙여 장난스럽게 영화보자고 문자한다.",
                    "혹시 차일지 모르니, \"요즘 영화 재미있는 것 많던데...\" 라고 운을 띄워 글을 보낸 뒤 여자의 대답을 기다린다.",
                    "\"친구가 영화티켓을 공짜로 줬는데..\" 여자가 부담스럽지 않게 선의의 거짓말로 문자한다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "회사 끝나고 가기에도 벅차다! 그냥 평소 복장 그대로!",
                    "패션엔 관심없다.. 다만 마음의 표시로 꽃 한송이를 사간다.",
                    "옷차림도 마음의 표시 중 하나다! 깔끔하게 정장, 오피스룩을 입고 나간다.",
                    "편한게 최고지! 청바지에 티셔츠"
            )),
            new ArrayList<String>(Arrays.asList(
                    "박력있게 먼저 주도해 나간다. \"요즘 이 영화가 흥행이래요? 이거 어때요?\"",
                    "남자는 역시 매너지. 여자에게 영화를 고르라고 선택권을 넘긴다.",
                    "평소 보고 싶은 영화가 마침 있다. 양해를 구하고 보고 싶은 영화를 선택한다.",
                    "뭐가 재밌는지 모르겠다. 최악의 선택은 막기 위해 직원에게 영화를 추천해달라고 말한다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "에잇, 이번 기회에 더욱 친해지자! 커플석",
                    "혹시 모를 스킨쉽을 대비해 맨 뒷자리 사이드석",
                    "일단 영화부터 잘 보는게 제일 중요하지! 정중앙자리",
                    "자리가 뭣이 중한디? 아무데나!"
            )),
            new ArrayList<String>(Arrays.asList(
                    "여자에게 솔직히 말하고 영화값을 내달라고 부탁한다.",
                    "여자를 붙잡고 일단 자리에서 벗어난다.",
                    "여자에게 사실을 말하고 다음번에 영화를 보자고 말한다.",
                    "왜 카드가 안되냐며 말도 안된다고 소리치며 오히려 황당하다는 듯한 태도를 보인다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "지금 시간이 몇시인데? 야식은 금물! 그냥 영화를 기다리며 팜플렛을 본다.",
                    "영화엔 팝콘과 콜라가 진리지. 더불어 핫도그, 나초도!",
                    "당연히 커피 아니야? 남자와 근처 카페에 들어가 커피를 주문한다.",
                    "별로 땡기지도 않고.. 그렇다고 안 땡기는 것도 아니고... 상대방의 의견에 따른다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "영화관엔 우리만 있는 것이 아니다! 여자에게 인기척을 보내며 핸드폰을 끄라고 말한다.",
                    "모른척 영화에 집중한다.",
                    "여자도 민망할 터이니 일단 인기척을 보내고 다시 영화에 집중한다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "영화는 엔딩크레딧까지지! 영화제작에 참여한 모든 이들의 이름이 나올때까지 가만히 앉아 스크린을 본다.",
                    "이름이 뭣이 중한디! 영화가 끝나자마자 바로 일어나 나갈 준비를 한다.",
                    "어쩌지? 다른 좌석 사람들의 행렬에 자연스럽게 일어나 합류한다.",
                    "조용히 여자의 눈치를 살피며 기다린다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "솔직한게 제일이다. 먼저 선수치자! '헤헤. 재밌을 줄 알았는데...'",
                    "데이트에 부정적인 말은 질색이다! 어떻게든 포장하자. \"배우가... 연기는 좋더라구요!\"",
                    "나만 재미없다 느꼈을 수 있다. 일단 상대방의 반응을 파악하자. \"영화.. 어떠셨어요?\"",
                    "묵묵부답. 여자가 먼저 이야기하길 기다린다."
            )),
            new ArrayList<String>(Arrays.asList(
                    "여자가 부담스럽지 않게, 집으로 데려다준다고 말한 뒤 함께 여자의 집으로 향한다.",
                    "근처 카페에 가 대화를 나누자고 이야기한다.",
                    "근처 맥주집으로 가 야식을 먹자고 한다.",
                    "아쉽지만, 여자에게 모범적인 남자로 보이는게 우선이다. 다음을 기약하며 오늘은 여기서 헤어진다. "
            ))
    ));

    public static ArrayList<ArrayList<Integer>> rM1 = new ArrayList<ArrayList<Integer>>(Arrays.asList(
            new ArrayList<Integer>(Arrays.asList(2, 2, 2, 2, 2)),
            new ArrayList<Integer>(Arrays.asList(3, 3, 3, 3, 3)),
            new ArrayList<Integer>(Arrays.asList(4, 4, 4, 4, 4)),
            new ArrayList<Integer>(Arrays.asList(5, 5, 5, 5, 5)),
            new ArrayList<Integer>(Arrays.asList(6, 6, 6, 6, 6)),
            new ArrayList<Integer>(Arrays.asList(7, 7, 7, 7, 7)),
            new ArrayList<Integer>(Arrays.asList(8, 8, 8, 8, 8)),
            new ArrayList<Integer>(Arrays.asList(9, 9, 9, 9, 9)),
            new ArrayList<Integer>(Arrays.asList(10, 10, 10, 10, 10)),
            new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0, 0))
    ));
    public static ArrayList<ArrayList<Integer>> rW1 = new ArrayList<ArrayList<Integer>>(Arrays.asList(
            new ArrayList<Integer>(Arrays.asList(2, 2, 2, 2, 2)),
            new ArrayList<Integer>(Arrays.asList(3, 3, 3, 3, 3)),
            new ArrayList<Integer>(Arrays.asList(4, 4, 4, 4, 4)),
            new ArrayList<Integer>(Arrays.asList(5, 5, 5, 5, 5)),
            new ArrayList<Integer>(Arrays.asList(6, 6, 6, 6, 6)),
            new ArrayList<Integer>(Arrays.asList(7, 7, 7, 7, 7)),
            new ArrayList<Integer>(Arrays.asList(8, 8, 8, 8, 8)),
            new ArrayList<Integer>(Arrays.asList(9, 9, 9, 9, 9)),
            new ArrayList<Integer>(Arrays.asList(10, 10, 10, 10, 10)),
            new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0, 0))
    ));

    public static ArrayList<String> questions2 = new ArrayList<>(Arrays.asList(
            "오랜만에 친구를 만나서 근황을 이야기했다. 친구는 이제 막 사귀게 된 남자와 고궁을 놀러간 이야기를 한다. “다음 주까지 경복궁 야간 개장한대. 밤에 보니까 또 분위기 있고 좋더라.” 고궁데이트! 당신은 그것이 굉장히 로맨틱하게 생각된다. 그러면서 자연스럽게 같이 가고 싶은 남자가 떠오른다. 당신은 당장 그에게 문자를 보낸다.",
            "일이 너무 많아서 그녀에게 연락할 틈이 없었다. 야근하는 중 그녀에게 온 문자 메시지는 다시 읽어보아도 데이트 신청이었다. 일단 당신은 빠른 속도로 수락한다. 그리고 근처의 다른 데이트 코스를 찾는다. 그러던 중 보이는 전통시장. 당신은 다음 데이트 코스로 전통시장을 정한다. 하지만 곧 그녀의 취향을 잘 모른다는 것을 생각한다. 시끄러운 전통시장을 싫어할 수도 있지 않을까? 그 생각을 한 후 당신의 행동은?",
            "남자가 전통시장도 가는 것은 어떤지 제안한다. 고궁 데이트를 먼저 제안한 사람이 자신이니 일단 수락했다. 그러는 동안 당신이 한 생각은?",
            "약속 당일. 당신은 그녀를 만나러 밖으로 나가자마자 아이스크림을 동네 꼬마 아이와 부딪쳐 바지를 더럽히고 말았다. 빠르게 집으로 돌아가 옷을 갈아입고 나오는데 타려는 버스를 보는 앞에서 놓치고, 급한 마음에 열차를 잘못 타는 등 안 하던 실수를 한다. 당신은 약속시간에 늦을 것 같다. 이때 당신의 행동은?\n",
            "남자가 택시를 타고 도착한 것을 보았다. 당신은 무슨 생각이 드는가?",
            "헐레벌떡 뛰어 갔는데 다행히 그녀가 자신을 기다리고 있다. 그녀는 화를 참고 있는 것처럼 보인다. 그녀에게 당신은 어떻게 사과할 것인가?",
            "주변 커플들을 보니 한복을 입은 이들이 많다. 아마도 주변에 대여를 하는 곳이 있는 모양이었다. 그때 당신이 하는 생각은?",
            "주변 커플들을 보니 한복을 입은 이들이 많다. 아마도 주변에 대여를 하는 곳이 있는 모양이었다. 그때 당신이 하는 생각은?",
            "한복을 입은 사람 중에 낯익은 얼굴이 있다. 그녀는 바로 고등학교 동창. 그녀 옆에는 훤칠한 남자가 한복을 입고 서 있다. 그는 당신의 첫사랑. 물론 동창은 그녀가 좋아했다는 사실을 모르고 있었지만 배가 아프다. 그들은 당신에게 반갑게 인사한 후 옆의 남자가 누구인지를 묻는다. 뭐라고 대답할까?",
            "한복을 입은 한 커플이 그녀에게로 다가간다. 그녀의 지인인 모양이다. 그들은 그녀에게 반갑게 인사한 후 당신이 누구인지를 묻는다. 그녀는 난처한 눈치다. 당신은 뭐라고 대답을 대신해줄 것인가?\n",

            "대답도 하기 전부터 한복을 입은 커플은 당신과 그를 연인이라고 믿는 눈치였다. 동창인 여자는 갑자기 깨달았다는 듯이 그에게 다가가 “아, 혹시 **씨 아니세요?” 묻는다. 그건 그의 이름이 아니다. 당황한 당신은 뭐라고 말할 것인가?",
            "대답도 하기 전부터 한복을 입은 커플은 당신과 그녀를 연인이라고 믿는 눈치였다. 그녀의 동창은 갑자기 깨달았다는 듯이 당신에게 다가가 “아, 혹시 **씨 아니세요?” 묻는다. 그건 당신의 이름이 아니다. 당황한 당신은 어떻게 행동할 것인가?",
            "친구 커플과 인사한 후, 당신은 그와 단 둘이 남아 고궁을 산책한다. 잠시 어색한 기류. 남자는 그녀의 동창이 예쁘다고 말한다. 그때 당신의 기분은?",
            "친구 커플과 인사한 후, 당신은 그녀와 단 둘이 남아 고궁을 산책한다. 잠시 어색한 기류. 당신은 뭐라도 말해야 한다는 생각에 그녀의 동창이 예쁘다고 말했다. 그러고 나서 그녀의 기분이 좋지 않을 거라는 생각이 뒤늦게 들었다. 그 후 당신은 어떻게 행동할 것인가?\n",
            "날씨가 맑은 데다 바람도 선선하다. 주변에는 모두 커플들뿐. 다른 커플들처럼 어깨동무를 하거나 팔짱을 끼는 것은 아니어도 당신은 그와 함께 손을 잡고 걷고 싶다는 생각이 들었다. 당신은 어떻게 행동할 것인가?",
            "날씨가 맑은 데다 바람도 선선하다. 주변에는 모두 커플들뿐. 다른 커플들처럼 어깨동무를 하거나 팔짱을 끼는 것은 아니어도 당신은 그녀와 함께 손을 잡고 걷고 싶다는 생각이 들었다. 당신은 어떻게 행동할 것인가?\n",
            "전통시장에 가서 음식 냄새를 맡자 갑자기 배가 고팠다. 당신은 전통 시장에 가면 어떤 음식이 생각나는가?",
            "전통시장에 가서 음식 냄새를 맡자 갑자기 배가 고팠다. 당신은 전통 시장에 가면 어떤 음식이 생각나는가?",
            "전 부치는 냄새가 너무 맛있게 난다. 배가 너무 고파서 더는 걷기 힘들고 전집에서 간단하게 막걸리를 마시고 싶다. 그러나 당신은 남자의 취향을 모른다. 남자에게 뭐라 말할 것인가?",
            "전 부치는 냄새가 너무 맛있게 난다. 배가 너무 고파서 전집에서 간단하게 막걸리를 마시고 싶다. 그러나 당신은 여자의 취향을 모른다. 그때 여자가 먼저 빈대떡에 막걸리를 언급했다. 당신은 뭐라고 대답할 것인가?",

            "따끈하고 두툼한 빈대떡이 크게 한 접시 나왔다. 그것은 아주 먹음직스러워 보인다. 당신은 호감이 있는 사람과 함께 빈대떡과 막걸리를 먹을 때 어떻게 행동할 것인가?",
            "따끈하고 두툼한 빈대떡이 크게 한 접시 나왔다. 그것은 아주 먹음직스러워 보인다. 당신은 호감이 있는 사람과 함께 빈대떡과 막걸리를 먹을 때 어떻게 행동할 것인가?",
            "데이트가 끝나고 집으로 돌아가는 길. 당신은 버스 정류장으로 가야하고 남자는 지하철역으로 가야 한다. 대화를 하며 걷다 보니 버스 정류장과 지하철역 중간 지점에 다 왔다. 당신은 어떤 생각이 들까?\n",
            "데이트가 끝나고 집으로 돌아가는 길. 당신은 지하철역으로 가야하고 여자는 버스 정류장으로 가야 한다. 대화를 하며 걷다 보니 어느새 버스 정류장과 지하철역 중간 지점에 다 왔다. 당신은 어떤 생각이 들까?\n",
            "당신은 오늘의 데이트가 만족스러웠다. 그리고 상대방에 대한 호감도도 높아진 상태다. 막걸리를 마셔서인지, 가로등불 아래 남자가 평소보다 다정하고 멋있어 보인다. 당신은 남자와 진지하게 만나는 것도 나쁘지 않다고 생각한다. 각자의 집에 가기 직전 당신의 행동은?",
            "당신은 오늘의 데이트가 만족스러웠다. 그리고 상대방에 대한 호감도도 높아진 상태다. 막걸리를 마셔서인지, 가로등불 아래 여자가 평소보다 다정하고 예쁘게 보인다. 당신은 여자와 진지하게 만나는 것도 나쁘지 않다고 생각한다. 각자의 집에 가기 직전 당신의 행동은?"
    ));

    public static ArrayList<ArrayList<String>> a2 = new ArrayList<>(Arrays.asList(
            new ArrayList<>(Arrays.asList(
                    "이번에 친구가 고궁에 갔는데 좋았나 봐요. 나도 가고 싶다…….",
                    "우리 같이 고궁 갈래요?",
                    "주말에 쉬어요? 시간 되면 고궁 근처에서 볼까요?",
                    "고궁 가고 싶은데 함께 갈 사람이 없네요. 괜찮으면 같이 갈래요?"
            )),
            new ArrayList<>(Arrays.asList(
                    "그녀에게 다시 연락해서 전통시장을 가는 것은 어떤지 의견을 묻는다.",
                    "만나서 분위기를 보고 갈지 말지 정하기로 한다.",
                    "내가 보기에 그녀는 전통시장을 무조건 좋아할 것이다. 일단은 혼자 전통시장의 맛집과 루트를 알아 놓는다."
            )),
            new ArrayList<>(Arrays.asList(
                    "원래 전통시장을 좋아했다. 무조건 찬성!",
                    "시끄럽고 사람이 많은 곳은 질색이지만, 이번 한 번만 가주기로 하자.",
                    "장소는 중요하지 않다. 내가 좋아하는 사람과 함께라면.",
                    "미리 다음 코스까지 생각하다니 센스가 있다고 생각한다",
                    "자신도 전통시장에 대해서 더 조사해야겠다고 생각한다."
            )),
            new ArrayList<>(Arrays.asList(
                    "그녀를 기다리게 할 수는 없지. 택시를 타자.",
                    "일단 스마트폰으로 최대한 빨리 가는 루트를 찾아본다.",
                    "그녀에게 조금 늦을 것 같다고 미리 사과 문자를 보낸다."
            )),
            new ArrayList<>(Arrays.asList(
                    "그저 마냥 반갑다.",
                    "왜 택시를 타고 오지? 평소에도 택시를 자주 타나. 낭비라고 생각한다.",
                    "왜 택시를 타고 오지? 급한 일이 있었나? 어디서 온 것인지 궁금하다.",
                    "남자에게 차가 없구나, 생각한다."
            )),
            new ArrayList<>(Arrays.asList(
                    "가셨을 줄 알았어요. 기다려줘서 고마워요.",
                    "연락을 드렸어야 했는데, 오는 길에 일이 많았어요. 죄송해요. 일단 안에 들어가서 음료라도 드실래요?",
                    "잘 보이려고 이것저것 준비했는데 긴장해서인지 열차를 잘못 타는 안 하던 실수도 하게 되었네요. 죄송해요.",
                    "늦어서 정말 죄송해요. 대신 오늘 하루는 00씨 하자는 대로 할게요."
            )),
            new ArrayList<>(Arrays.asList(
                    "나도 한복 입고 길을 걷고 싶다. 다음에라도 꼭 입고 싶다.",
                    "옷을 빌려 입는 것이 신경 쓰일 텐데, 저 사람들은 대단하네.",
                    "걷고 데이트하기에는 거추장스러워 보인다. 나는 입지 말아야지.",
                    "그는 한복 데이트에 대해서 어떻게 생각할까?"
            )),
            new ArrayList<>(Arrays.asList(
                    "나도 한복 입고 길을 걷고 싶다. 다음에라도 꼭 입고 싶다.",
                    "옷을 빌려 입는 것이 신경 쓰일 텐데, 저 사람들은 대단하네.",
                    "걷고 데이트하기에는 거추장스러워 보인다. 나는 입지 말아야지.",
                    "그녀는 한복 데이트에 대해서 어떻게 생각할까?"
            )),
            new ArrayList<>(Arrays.asList(
                    "(어색하게) 아아……, 그냥 아는 사람이야.",
                    "친한 친구야.",
                    "인사해. 최근에 만나고 있는 사람이야.",
                    "(능청스럽게) 아직 사귀는 건 아니지만 잘 되어가고 있는 사람이야."
            )),
            new ArrayList<>(Arrays.asList(
                    "아아……, 그냥 어떻게 아는 사이입니다, 저흰.",
                    "저희는 친구에요.",
                    "반갑습니다. 00의 남자친구입니다.",
                    "(능청스럽게) 안녕하세요. 아직 사귀는 건 아니지만 곧 00씨와 사귀게 될 사람입니다."
            )),
            new ArrayList<>(Arrays.asList(
                    "얘는 도대체 몇 년 전 이야기를 하는 거니.",
                    "**씨? 그 사람이 누구야? 호호, 누구랑 헷갈리는 거야?",
                    "걔는 그냥 대학교 동기라고 했잖아.",
                    "내 앞에서 그 사람 이름 꺼내지도 말라고 그랬지.",
                    "내가 일편단심 민들레인 줄 아니?"
            )),
            new ArrayList<>(Arrays.asList(
                    "그녀가 상황을 정리하길 기다린다.",
                    "그녀의 동창에게 묻는다. “** 씨는 누구에요?”",
                    "그녀에게 묻는다. “** 씨는 누구에요?”",
                    "그냥 자기소개를 한다. “아, 제 이름은 @@입니다.”",
                    "그녀의 분위기를 살핀 후, 못 들었다는 듯 질문을 되묻는다."
            )),
            new ArrayList<>(Arrays.asList(
                    "별 생각 없다.",
                    "내 앞에서 다른 이성을 칭찬하는 것이 기분 나쁘다.",
                    "동창을 칭찬한 것이니 기분이 좋다.",
                    "일단 맞장구 친 후, 미남인 동창의 남자친구에 대해 이야기하며 사소한(?) 복수를 한다."
            )),
            new ArrayList<>(Arrays.asList(
                    "또 다시 생각해보니 동창을 칭찬한 것인데 기분이 나쁠까, 싶다. 다른 이야기로 넘어간다.",
                    "“예쁜 사람은 예쁜 사람이랑 친구인가 봐요.” 말한다.",
                    "“물론 00씨가 더 예쁘지만요.” 말한다.",
                    "이미 한 말이라 어쩔 수 없다고 생각하고 침묵한다."
            )),
            new ArrayList<>(Arrays.asList(
                    "“손잡아도 돼요?” 묻는다.",
                    "말없이 손을 잡는다.",
                    "손을 잡고 싶지만 그가 먼저 잡을 때까지 기다린다.",
                    "잡고 나서 당돌하게 묻는다. “여자가 먼저 잡으면 조금 그래요? 싫으면 놓을까요?”"
            )),
            new ArrayList<>(Arrays.asList(
                    "“손잡아도 돼요?” 묻는다.",
                    "말없이 손을 잡는다.",
                    "손을 잡고 싶지만 그녀가 먼저 잡을 때까지 기다린다.",
                    "잡고 나서 당돌하게 묻는다. “혹시 갑자기 손잡아서 놀랐어요? 싫으면 놓을까요?”"
            )),
            new ArrayList<>(Arrays.asList(
                    "빈대떡을 비롯한 각종 전",
                    "김밥과 떡볶이와 순대",
                    "회",
                    "육회",
                    "칼국수나 잔치국수"
            )),
            new ArrayList<>(Arrays.asList(
                    "빈대떡을 비롯한 각종 전",
                    "김밥과 떡볶이와 순대",
                    "회",
                    "육회",
                    "칼국수나 잔치국수"
            )),
            new ArrayList<>(Arrays.asList(
                    "빈대떡 부치는 냄새가 정말 맛있게 나네요……. 막걸리랑 정말 어울리겠다.",
                    "빈대떡에 막걸리 어떠세요?",
                    "빈대떡에 막걸리 좋아하는 여자는 어때요?"
            )),
            new ArrayList<>(Arrays.asList(
                    "저도 그 생각했는데. 여기 이 집으로 들어갈까요?",
                    "빈대떡 좋아하세요? 알아 놓은 맛집이 있어요.",
                    "저도 빈대떡 진짜 좋아해요. 예전에 갔던 가게가 있는데 그곳으로 갈까요?"
            )),
            new ArrayList<>(Arrays.asList(
                    "평소와 다름없이 먹는다. 열심히 전을 찢어 열심히 먹는다. 그러면서 시원한 막걸리도 열심히 들이켜 준다.",
                    "평소보다는 조금 먹는다. 막걸리도 눈치껏 적당히 마신다.",
                    "전을 찢어서 상대방 앞접시에 올려놓아주고 맛을 묻는다. “여기 빈대떡 어때요?”",
                    "대화 중간에 “짠~”을 외치며 막걸리를 많이 마신다."
            )),
            new ArrayList<>(Arrays.asList(
                    "평소와 다름없이 먹는다. 열심히 전을 찢어 열심히 먹는다. 그러면서 시원한 막걸리도 열심히 들이켜 준다.",
                    "평소보다는 조금 먹는다. 막걸리도 눈치껏 적당히 마신다.",
                    "전을 찢어서 상대방 앞접시에 올려놓아주고 맛을 묻는다. “여기 빈대떡 어때요?”",
                    "대화 중간에 “짠~”을 외치며 막걸리를 많이 마신다."
            )),
            new ArrayList<>(Arrays.asList(
                    "남자가 여자를 데려다 줘야지. 남자가 버스정류장까지 같이 걸어 줄 것이다.",
                    "여자가 남자를 데려다 줄 수도 있지. 하지만 오늘은 남자가 데려다주었으면 좋겠다.",
                    "여자가 남자를 데려다 줄 수도 있지. 내가 지하철역으로 데려다준다.",
                    "좀 더 이야기하고 싶다. 조금 돌아가더라도 지하철로 가야겠다.",
                    "늦은 시간도 아니고 거리도 밝은데 뭘 데려다주고 말고 한담. 여기서 각자 갈 길을 가야지."
            )),
            new ArrayList<>(Arrays.asList(
                    "남자가 여자를 데려다 줘야지. 내가 버스정류장까지 같이 걸어 줄 것이다.",
                    "여자가 남자를 데려다 줄 수도 있지. 하지만 오늘은 내가 데려다 주는 것이 나을 것 같다.",
                    "여자가 남자를 데려다 줄 수도 있지. 여자가 지하철역까지 같이 걸어주었으면.",
                    "좀 더 이야기하고 싶다. 조금 돌아가더라도 버스로 가야겠다.",
                    "늦은 시간도 아니고 거리도 밝은데 뭘 데려다주고 말고 한담. 여기서 각자 갈 길을 가야지."
            )),
            new ArrayList<>(Arrays.asList(
                    "솔직하게 자신의 마음을 전한다. “오늘 재밌었어요. 저는 @@씨와 진지하게 만나고 싶은데, @@씨는 어떠세요?”",
                    "아직은 더 만나봐야 할 것 같다. 그 자리에서 다음 약속을 정한다. “오늘 즐거웠어요. 우리 다음에는 또 언제 만나죠?”",
                    "상대방의 마음에 확신이 서지 않는다. 신호를 주되 그가 먼저 적극적으로 표현할 때까지 기다린다. “오늘 즐거웠어요. 안녕히 들어가세요. 다음에 또 연락해요.”"
            )),
            new ArrayList<>(Arrays.asList(
                    "솔직하게 자신의 마음을 전한다. “오늘 재밌었어요. 저는 00씨와 진지하게 만나고 싶은데, 00씨는 어떠세요?”",
                    "아직은 더 만나봐야 할 것 같다. 그 자리에서 다음 약속을 정한다. “오늘 즐거웠어요. 우리 다음에는 또 언제 만나죠?”",
                    "상대방의 마음에 확신이 서지 않는다. 신호를 주되 그녀가 먼저 적극적으로 표현할 때까지 기다린다. “오늘 즐거웠어요. 조심히 들어가세요. 집 도착하면 연락주세요.”"
            ))
    ));

    public static ArrayList<ArrayList<Integer>> r2 = new ArrayList<ArrayList<Integer>>(Arrays.asList(
            new ArrayList<Integer>(Arrays.asList(2, 2, 2, 2, 2)),
            new ArrayList<Integer>(Arrays.asList(3, 3, 3, 3, 3)),
            new ArrayList<Integer>(Arrays.asList(4, 4, 4, 4, 4)),
            new ArrayList<Integer>(Arrays.asList(5, 5, 5, 5, 5)),
            new ArrayList<Integer>(Arrays.asList(6, 6, 6, 6, 6)),
            new ArrayList<Integer>(Arrays.asList(7, 7, 7, 7, 7)),
            new ArrayList<Integer>(Arrays.asList(8, 8, 8, 8, 8)),
            new ArrayList<Integer>(Arrays.asList(9, 9, 9, 9, 9)),
            new ArrayList<Integer>(Arrays.asList(10, 10, 10, 10, 10)),
            new ArrayList<Integer>(Arrays.asList(11, 11, 11, 11, 11)),

            new ArrayList<Integer>(Arrays.asList(12, 12, 12, 12, 12)),
            new ArrayList<Integer>(Arrays.asList(13, 13, 13, 13, 13)),
            new ArrayList<Integer>(Arrays.asList(14, 14, 14, 14, 14)),
            new ArrayList<Integer>(Arrays.asList(15, 15, 15, 15, 15)),
            new ArrayList<Integer>(Arrays.asList(16, 16, 16, 16, 16)),
            new ArrayList<Integer>(Arrays.asList(17, 17, 17, 17, 17)),
            new ArrayList<Integer>(Arrays.asList(18, 18, 18, 18, 18)),
            new ArrayList<Integer>(Arrays.asList(19, 19, 19, 19, 19)),
            new ArrayList<Integer>(Arrays.asList(20, 20, 20, 20, 20)),
            new ArrayList<Integer>(Arrays.asList(21, 21, 21, 21, 21)),

            new ArrayList<Integer>(Arrays.asList(22, 22, 22, 22, 22)),
            new ArrayList<Integer>(Arrays.asList(23, 23, 23, 23, 23)),
            new ArrayList<Integer>(Arrays.asList(24, 24, 24, 24, 24)),
            new ArrayList<Integer>(Arrays.asList(25, 25, 25, 25, 25)),
            new ArrayList<Integer>(Arrays.asList(26, 26, 26, 26, 26)),
            new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0, 0))
    ));

    public static String dating(int id, boolean is, int qn, int an, int sex) {
        if (id == 1) {
            if (is) {
                JSONObject jd = new JSONObject();
                try {
                    jd.put("ddlId", "1");
                    jd.put("dlId", "1");
                    jd.put("ddlQuestionNumber", "1");
                    jd.put("ddlQuestion", sex == 0 ? questionsW1.get(0) + "" : questionsM1.get(0) + "");
                    jd.put("ddlSex", sex + "");
                    jd.put("ddlACount", sex == 0 ? aW1.get(0).size() + "" : aM1.get(0).size() + "");
                    jd.put("ddlA1", sex == 0 ? aW1.get(0).get(0) + "" : aM1.get(0).get(0) + "");
                    jd.put("ddlA1R", sex == 0 ? rW1.get(0).get(0) + "" : rM1.get(0).get(0) + "");
                    jd.put("ddlA2", sex == 0 ? aW1.get(0).get(1) + "" : aM1.get(0).get(1) + "");
                    jd.put("ddlA2R", sex == 0 ? rW1.get(0).get(1) + "" : rM1.get(0).get(1) + "");
                    jd.put("ddlA3", sex == 0 ? aW1.get(0).get(2) + "" : aM1.get(0).get(2) + "");
                    jd.put("ddlA3R", sex == 0 ? rW1.get(0).get(2) + "" : rM1.get(0).get(2) + "");
                    jd.put("ddlA4", sex == 0 ? aW1.get(0).get(3) + "" : aM1.get(0).get(3) + "");
                    jd.put("ddlA4R", sex == 0 ? rW1.get(0).get(3) + "" : rM1.get(0).get(3) + "");
                    jd.put("ddlA5", "");
                    jd.put("ddlA5R", "");
                    jd.put("ddlRegDate", "2016-12-08 11:38:22");
                    jd.put("result", "0");
                    jd.put("msg", "success");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jd.toString();
            } else {
                JSONObject jd = new JSONObject();
                int q = sex == 0 ? rW1.get(qn - 1).get(an - 1) - 1 : rM1.get(qn - 1).get(an - 1) - 1;
                if (q == -1) {
                    try {
                        jd.put("result", "2");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return jd.toString();
                }
                try {
                    Log.e("q", q + "/" + qn + "/" + an);
                    jd.put("ddlId", "1");
                    jd.put("dlId", "1");
                    jd.put("ddlQuestionNumber", (qn + 1) + "");
                    jd.put("ddlQuestion", sex == 0 ? questionsW1.get(q) + "" : questionsM1.get(q) + "");
                    jd.put("ddlSex", sex + "");
                    jd.put("ddlACount", sex == 0 ? aW1.get(q).size() + "" : aM1.get(q).size() + "");
                    jd.put("ddlA1", sex == 0 ? aW1.get(q).get(0) + "" : aM1.get(q).get(0) + "");
                    jd.put("ddlA1R", sex == 0 ? rW1.get(q).get(0) + "" : rM1.get(q).get(0) + "");
                    jd.put("ddlA2", sex == 0 ? aW1.get(q).get(1) + "" : aM1.get(q).get(1) + "");
                    jd.put("ddlA2R", sex == 0 ? rW1.get(q).get(1) + "" : rM1.get(q).get(1) + "");
                    jd.put("ddlA3", sex == 0 ? aW1.get(q).get(2) + "" : aM1.get(q).get(2) + "");
                    jd.put("ddlA3R", sex == 0 ? rW1.get(q).get(2) + "" : rM1.get(q).get(2) + "");
                    jd.put("ddlA4", sex == 0 ? aW1.get(q).get(3) + "" : aM1.get(q).get(3) + "");
                    jd.put("ddlA4R", sex == 0 ? rW1.get(q).get(3) + "" : rM1.get(q).get(3) + "");
                    jd.put("ddlA5", "");
                    jd.put("ddlA5R", "");
                    jd.put("ddlRegDate", "2016-12-08 11:38:22");
                    jd.put("result", "0");
                    jd.put("msg", "success");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jd.toString();
            }
        } else {
            if (is) {
                JSONObject jd = new JSONObject();
                try {
                    int start = 0;
                    if (sex == 0) {
                        start = 0;
                    } else {
                        start = 1;
                    }
                    jd.put("ddlId", "2");
                    jd.put("dlId", "2");
                    jd.put("ddlQuestionNumber", "1");
                    jd.put("ddlQuestion", questions2.get(start) + "");
                    jd.put("ddlSex", sex + "");
                    jd.put("ddlACount", a2.get(start).size() + "");
                    for (int i = 0; i < 5; i++) {
                        if (i + 1 > a2.get(start).size()) {
                            jd.put("ddlA" + (i + 1), "");
                            jd.put("ddlA" + (i + 1) + "R", "");
                        } else {
                            jd.put("ddlA" + (i + 1), sex == 0 ? a2.get(start).get(i) + "" : a2.get(start).get(i) + "");
                            jd.put("ddlA" + (i + 1) + "R", sex == 0 ? r2.get(start).get(i) + "" : r2.get(start).get(i) + "");
                        }
                    }
                    jd.put("ddlRegDate", "2016-12-08 11:38:22");
                    jd.put("result", "0");
                    jd.put("msg", "success");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jd.toString();
            } else {
                JSONObject jd = new JSONObject();
                qn = qn + 1;
                int q = r2.get(qn - 1).get(an - 1) - 1;
                if (q == -1) {
                    try {
                        jd.put("result", "2");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return jd.toString();
                }
                try {
                    Log.e("q", q + "/" + qn + "/" + an);
                    jd.put("ddlId", "1");
                    jd.put("dlId", "1");
                    jd.put("ddlQuestionNumber", (qn + 1) + "");
                    jd.put("ddlQuestion", sex == 0 ? questions2.get(q) + "" : questions2.get(q) + "");
                    jd.put("ddlSex", sex + "");
                    jd.put("ddlACount", sex == 0 ? a2.get(q).size() + "" : a2.get(q).size() + "");
                    for (int i = 0; i < 5; i++) {
                        if (i + 1 > a2.get(q).size()) {
                            jd.put("ddlA" + (i + 1), "");
                            jd.put("ddlA" + (i + 1) + "R", "");
                        } else {
                            jd.put("ddlA" + (i + 1), sex == 0 ? a2.get(q).get(i) + "" : a2.get(q).get(i) + "");
                            jd.put("ddlA" + (i + 1) + "R", sex == 0 ? r2.get(q).get(i) + "" : r2.get(q).get(i) + "");
                        }
                    }
                    jd.put("ddlRegDate", "2016-12-08 11:38:22");
                    jd.put("result", "0");
                    jd.put("msg", "success");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jd.toString();
            }
        }

    }
}
