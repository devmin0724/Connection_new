package com.tambourine.devmin.connection.ui.popup;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.MinActivity;
import com.tambourine.devmin.connection.vo.ConnectionDetailItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmAgainPopupActivity extends MinActivity {

    @BindView(R.id.tv_confirm_again_popup_title)
    TextView tvTitle;
    @BindView(R.id.tv_confirm_again_popup_antenna_count)
    TextView tvAntennaCount;
    @BindView(R.id.tv_confirm_again_popup_body)
    TextView tvBody;

    @BindView(R.id.lay_confirm_again_popup_yes)
    LinearLayout layYes;
    @BindView(R.id.lay_confirm_again_popup_no)
    LinearLayout layNo;

    private int type = 0, antennaCount = 0;
    ConnectionDetailItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_again_popup);
        ButterKnife.bind(this);

        //TODO 0:데이트신청 1:데이트취소 2:데이트수락 3:데이트거절
        //TODO 4:채팅신청 5:채팅취소 6:채팅수락 7:채팅거절

        try {
            type = getIntent().getExtras().getInt("type");
            item = (ConnectionDetailItem) getIntent().getExtras().get("item");
        } catch (NullPointerException e) {
            finish();
            overridePendingTransition(0, 0);
        }
        switch (type) {
            case 0:
                tvTitle.setText("데이트 신청");
                tvBody.setText("데이트 신청을 하면 안테나가 사용대기 상태가 됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 데이트 신청
                    }
                });
                break;
            case 1:
                tvTitle.setText("데이트 취소");
                tvBody.setText("데이트 신청을 취소하면 사용대기 안테나가 반환되며 상대방에게 취소알림이 전달됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 데이트 취소
                    }
                });
                break;
            case 2:
                tvTitle.setText("데이트 수락");
                tvBody.setText("데이트를 수락하면 안테나가 소모되며 즉시 데이트가 진행됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 데이트 수락
                    }
                });
                break;
            case 3:
                tvTitle.setText("데이트 거절");
                tvBody.setText("데이트를 거절하면 거절알림이 상대방에게 전달됩니다.");
                tvAntennaCount.setText("0");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 데이트 거절
                    }
                });
                break;
            case 4:
                tvTitle.setText("채팅 신청");
                tvBody.setText("채팅을 신청하면 안테나가 사용대기 상태가 됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 채팅 신청
                    }
                });
                break;
            case 5:
                tvTitle.setText("채팅 취소");
                tvBody.setText("채팅 신청을 취소하면 사용대기 안테나가 반환되며 상대방에게 취소알림이 전달됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 채팅 취소
                    }
                });
                break;
            case 6:
                tvTitle.setText("채팅 수락");
                tvBody.setText("채팅을 수락하면 안테나가 소모되며 즉시 채팅이 시작됩니다.");
                tvAntennaCount.setText(item.getAntennaCount() + "");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 채팅 수락
                    }
                });
                break;
            case 7:
                tvTitle.setText("채팅 거절");
                tvBody.setText("채팅을 거절하면 거절알림이 상대방에게 전달됩니다.");
                tvAntennaCount.setText("0");

                layYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 채팅 거절
                    }
                });
                break;
        }
        layNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);
            }
        });
    }
}
