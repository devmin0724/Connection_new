package com.tambourine.devmin.connection.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.fragment.ConnectionFindFragment;
import com.tambourine.devmin.connection.fragment.ConnectionListFragment;
import com.tambourine.devmin.connection.fragment.SettingsFragment;
import com.tambourine.devmin.connection.fragment.ShopFragment;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.BackPressCloseHandler;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MinCompatActivity {

    @BindView(R.id.dl_activity_main_drawer)
    DrawerLayout dlDrawer;
    @BindView(R.id.imv_main_menu)
    ImageView imvMenu;
    @BindView(R.id.imv_main_retest)
    ImageView imvReTest;

    @BindView(R.id.vp_main)
    ViewPager viewPager;
    @BindView(R.id.lay_main_top_1)
    LinearLayout layTop1;
    @BindView(R.id.imv_main_top_1)
    ImageView imvTop1;
    @BindView(R.id.tv_main_top1)
    TextView tvTop1;
    @BindView(R.id.lay_main_top_2)
    LinearLayout layTop2;
    @BindView(R.id.imv_main_top_2)
    ImageView imvTop2;
    @BindView(R.id.tv_main_top2)
    TextView tvTop2;
    @BindView(R.id.lay_main_top_3)
    LinearLayout layTop3;
    @BindView(R.id.imv_main_top_3)
    ImageView imvTop3;
    @BindView(R.id.tv_main_top3)
    TextView tvTop3;
    @BindView(R.id.lay_main_top_4)
    LinearLayout layTop4;
    @BindView(R.id.imv_main_top_4)
    ImageView imvTop4;
    @BindView(R.id.tv_main_top4)
    TextView tvTop4;

    @BindView(R.id.lay_main_side_menu)
    LinearLayout layMenu;
    @BindView(R.id.tv_main_menu_login_type)
    TextView tvMenuLoginType;
    @BindView(R.id.tv_main_menu_email)
    TextView tvMenuEmail;
    @BindView(R.id.tv_main_menu_age)
    TextView tvMenuAge;
    @BindView(R.id.tv_main_menu_location)
    TextView tvMenuLocation;
    @BindView(R.id.tv_main_menu_notification_count)
    TextView tvMenuNotificationCount;
    @BindView(R.id.tv_main_menu_antenna_count)
    TextView tvMenuAntennaCount;
    @BindView(R.id.tv_main_menu_stand_by_antenna_count)
    TextView tvMenuStandByAntennaCount;
    @BindView(R.id.tv_main_menu_is_fixed_charge)
    TextView tvMenuIsFixedCharge;

    @BindView(R.id.imv_main_menu_sex)
    ImageView imvMenuSex;
    @BindView(R.id.imv_main_menu_antenna_new)
    ImageView imvMenuAntennaNew;
    @BindView(R.id.imv_main_menu_notification_new)
    ImageView imvMenuNotificationNew;

    @BindView(R.id.lay_main_menu_antenna)
    LinearLayout layMenuAntenna;
    @BindView(R.id.lay_main_menu_notification)
    LinearLayout layMenuNotification;
    @BindView(R.id.lay_main_menu_connection_find_list)
    LinearLayout layMenuConnectionFindList;
    @BindView(R.id.lay_main_menu_connection_list)
    LinearLayout layMenuConnectionList;
    @BindView(R.id.lay_main_menu_chatting_list)
    LinearLayout layMenuChattingList;
    @BindView(R.id.lay_main_menu_shop)
    LinearLayout layMenuShop;
    @BindView(R.id.lay_main_menu_settings)
    LinearLayout layMenuSettings;
    @BindView(R.id.lay_main_menu_modify_info)
    LinearLayout layMenuModifyInfo;
    @BindView(R.id.lay_main_menu_inquiry_list)
    LinearLayout layMenuInquiry;

    ArrayList<LinearLayout> lays;
    ArrayList<ImageView> imvs;
    ArrayList<TextView> tvs;

    private BackPressCloseHandler backPressCloseHandler;
    private FragmentPagerItemAdapter adapter;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ac = this;

        backPressCloseHandler = new BackPressCloseHandler(this);

//        pages = new FragmentPagerItems(this);

        adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("인연찾기", ConnectionFindFragment.class)
                .add("인연목록", ConnectionListFragment.class)
                .add("구매샵", ShopFragment.class)
                .add("설정", SettingsFragment.class)
                .create());

//        pages.add(FragmentPagerItem.of("인연찾기", ConnectionFindFragment.class));
//        pages.add(FragmentPagerItem.of("인연목록", ConnectionListFragment.class));
//        pages.add(FragmentPagerItem.of("구매샵", ShopFragment.class));
//        pages.add(FragmentPagerItem.of("설정", SettingsFragment.class));

        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(5);

//        if (Build.VERSION.SDK_INT >= 19) {
//            WebView.setWebContentsDebuggingEnabled(true);
//        }
//
//        UnityAds.setListener(ConnectionController.unityAdsListener);
//        UnityAds.setDebugMode(true);
//
//        MediationMetaData mediationMetaData = new MediationMetaData(this);
//        mediationMetaData.setName("mediationPartner");
//        mediationMetaData.setVersion("v12345");
//        mediationMetaData.commit();
//
//        MetaData debugMetaData = new MetaData(this);
//        debugMetaData.set("test.debugOverlayEnabled", true);
//        debugMetaData.commit();
//
//        UnityAds.initialize(this, getResources().getString(R.string.unity_ads_game_id), ConnectionController.unityAdsListener, false);
//
//        SharedPreferences preferences = getSharedPreferences("Settings", MODE_PRIVATE);
//        SharedPreferences.Editor preferencesEdit = preferences.edit();
//        preferencesEdit.putString("gameId", getResources().getString(R.string.unity_ads_game_id));
//        preferencesEdit.commit();

//        startActivity(new Intent(MainActivity.this, ADActivity.class));

        lays = new ArrayList<>();
        lays.add(layTop1);
        lays.add(layTop2);
        lays.add(layTop3);
        lays.add(layTop4);

        imvs = new ArrayList<>();
        imvs.add(imvTop1);
        imvs.add(imvTop2);
        imvs.add(imvTop3);
        imvs.add(imvTop4);

        tvs = new ArrayList<>();
        tvs.add(tvTop1);
        tvs.add(tvTop2);
        tvs.add(tvTop3);
        tvs.add(tvTop4);

        for (int i = 0; i < lays.size(); i++) {
            final int finalI = i;
            lays.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    initPager(finalI);
                    viewPager.setCurrentItem(finalI, true);
                }
            });
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                initPager(position);
            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        layMenuAntenna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AntennaListActivity.class));
            }
        });

        layMenuNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
            }
        });

        layMenuConnectionFindList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
                initPager(0);
                dlDrawer.closeDrawer(layMenu);
            }
        });
        layMenuConnectionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
                initPager(1);
                dlDrawer.closeDrawer(layMenu);
            }
        });

        layMenuShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
                initPager(2);
                dlDrawer.closeDrawer(layMenu);
            }
        });

        layMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(3);
                initPager(3);
                dlDrawer.closeDrawer(layMenu);
            }
        });

        layMenuModifyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ModifyInfoActivity.class));
                overridePendingTransition(0, 0);
            }
        });

        layMenuInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, InquiryListActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        layMenuChattingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ChattingListActivity.class));
                overridePendingTransition(0, 0);
            }
        });

        imvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dlDrawer.isDrawerOpen(layMenu)) {
                    dlDrawer.closeDrawer(layMenu);
                } else {
                    dlDrawer.openDrawer(layMenu);
                }
            }
        });

        imvReTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LoveTendencyTestIntroActivity.class));
                overridePendingTransition(0, 0);
                finish();
            }
        });

//        loadInfo();
    }

    private void initPager(int a) {
        imvTop1.setVisibility(View.INVISIBLE);
        imvTop2.setVisibility(View.INVISIBLE);
        imvTop3.setVisibility(View.INVISIBLE);
        imvTop4.setVisibility(View.INVISIBLE);

        tvTop1.setTextColor(0x55ffffff);
        tvTop2.setTextColor(0x55ffffff);
        tvTop3.setTextColor(0x55ffffff);
        tvTop4.setTextColor(0x55ffffff);

        imvs.get(a).setVisibility(View.VISIBLE);
        tvs.get(a).setTextColor(0xffffffff);
    }

    private void loadInfo() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");

        new APITaskNew(this, params, ConnectionController.API_GET_USER_INFO, loadInfoResult).execute();

    }

    @Override
    public void onBackPressed() {
        if (dlDrawer.isDrawerOpen(layMenu)) {
            dlDrawer.closeDrawer(layMenu);
        } else {
            backPressCloseHandler.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadInfo();
    }

    private StringTransMethod loadInfoResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                switch (Integer.parseInt(jd.getString("mJoinType"))) {
                    case 10002:
                        tvMenuLoginType.setText("페이스북 로그인");
                        tvMenuLoginType.setTextColor(getResources().getColor(R.color.facebook_border_color));
                        break;
                    case 10003:
                        tvMenuLoginType.setText("구글 로그인");
                        tvMenuLoginType.setTextColor(getResources().getColor(R.color.google_border_color));
                        break;
                    case 10004:
                        tvMenuLoginType.setText("카카오 로그인");
                        tvMenuLoginType.setTextColor(getResources().getColor(R.color.kakao_border_color));
                        break;
                    default:
                        tvMenuLoginType.setText("이메일 로그인");
                        tvMenuLoginType.setTextColor(getResources().getColor(R.color.main_txt_color));
                        break;
                }

                imvMenuSex.setImageResource(Integer.parseInt(jd.getString("mSex")) == 0 ? R.drawable.icon_main_woman : R.drawable.icon_main_man);

                tvMenuEmail.setText(jd.getString("mName"));
                Calendar _c = Calendar.getInstance();
                int age = (_c.get(Calendar.YEAR) + 1) - Integer.parseInt(jd.getString("mBirth"));
                tvMenuAge.setText(age + "세");
                int loc = Integer.parseInt(jd.getString("mLoc"));
                tvMenuLocation.setText(getResources().getStringArray(R.array.location)[loc]);

                tvMenuNotificationCount.setText(jd.getInt("notificationCount") + "");
                tvMenuAntennaCount.setText(jd.getString("mAntenna"));

                if (jd.getInt("isNotificationNew") == 1) {
                    imvMenuNotificationNew.setVisibility(View.VISIBLE);
                } else {
                    imvMenuNotificationNew.setVisibility(View.INVISIBLE);
                }
                imvMenuAntennaNew.setVisibility(View.INVISIBLE);
//                if (tvMenuAntennaCount.getText().toString().equals("0")) {
//                    imvMenuAntennaNew.setVisibility(View.INVISIBLE);
//                } else {
//                    imvMenuAntennaNew.setVisibility(View.VISIBLE);
//                }

                tvMenuStandByAntennaCount.setText("사용예정인 안테나가 " + jd.getInt("intendedUseAntenna") + "개 있습니다.");

                tvMenuIsFixedCharge.setVisibility(View.GONE);
//                if (((int) Math.random() * 2 + 1) % 2 == 0) {
//                    tvMenuIsFixedCharge.setVisibility(View.GONE);
//                } else {
//                    tvMenuIsFixedCharge.setVisibility(View.VISIBLE);
//                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
