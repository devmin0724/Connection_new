package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class ConnectionDetailPopupItem {
    private String id = "";
    private String q = "";
    private String answer = "";
    private boolean sex = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }
}
