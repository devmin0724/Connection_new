package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class InquiryListItem {
    private String id = "";
    private String q = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }
}
