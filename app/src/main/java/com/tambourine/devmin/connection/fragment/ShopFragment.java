package com.tambourine.devmin.connection.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.ShopActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;

/**
 * Created by devmin on 2016-11-28.
 */

public class ShopFragment extends Fragment {

    LinearLayout layBuy;
    LinearLayout layFreeCharge;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_shop, container, false);
        layBuy = (LinearLayout) rootView.findViewById(R.id.lay_shop_buy);
        layFreeCharge = (LinearLayout) rootView.findViewById(R.id.lay_shop_free_charge);

        layBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), ShopActivity.class));
                getActivity().overridePendingTransition(0, 0);
            }
        });

        layFreeCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 무료충전소 이동
                Toast.makeText(getContext().getApplicationContext(), "준비중", Toast.LENGTH_SHORT).show();
            }
        });

        ConnectionController.setGlobalFont(rootView);

        return rootView;

    }
}
