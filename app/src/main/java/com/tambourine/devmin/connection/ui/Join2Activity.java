package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidwheelview.dusunboy.github.com.library.dialog.OnTextSetListener;
import androidwheelview.dusunboy.github.com.library.dialog.WheelViewDialog;

public class Join2Activity extends MinCompatActivity {

    public static final int LOCATION_SELECT_RESULT = 1111;
    public static final int BIRTH_SELECT_RESULT = 1112;

    TextView tvLocation, tvBirth;
    LinearLayout layBirth_, layLocation_, laySex, layNext, layCheck1, layCheck2;
    EditText edtName;
    ImageView imvCheck1, imvCheck2;

    String email, pass, location = "", birth = "", sex = "", type = "";
    int locationNum = 0;

    WheelViewDialog birthDialog, locationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join2);

        tvLocation = (TextView) findViewById(R.id.tv_join2_location);
        tvBirth = (TextView) findViewById(R.id.tv_join2_birth);

        layBirth_ = (LinearLayout) findViewById(R.id.lay_join2_birth_);
        layLocation_ = (LinearLayout) findViewById(R.id.lay_join2_location_);
        laySex = (LinearLayout) findViewById(R.id.lay_join2_sex);
        layNext = (LinearLayout) findViewById(R.id.lay_join2_next);
        layCheck1 = (LinearLayout) findViewById(R.id.lay_join2_check_1);
        layCheck2 = (LinearLayout) findViewById(R.id.lay_join2_check_2);

        edtName = (EditText) findViewById(R.id.edt_join2_name);
        imvCheck1 = (ImageView) findViewById(R.id.imv_join2_check_1);
        imvCheck2 = (ImageView) findViewById(R.id.imv_join2_check_2);

        birthDialog = new WheelViewDialog(Join2Activity.this, WheelViewDialog.ONE_LEVEL);
        birthDialog.setData(ConnectionController.births);
        birthDialog.setPositiveButton("확인", new OnTextSetListener() {
            @Override
            public void onTextSet(WheelViewDialog wheelViewDialog, String text) {
                wheelViewDialog.dismiss();
                birth = text;
                tvBirth.setText(birth);
            }
        });
        birthDialog.setTitle("출생연도 선택");

        locationDialog = new WheelViewDialog(Join2Activity.this, WheelViewDialog.ONE_LEVEL);
        locationDialog.setData(ConnectionController.locations);
        locationDialog.setPositiveButton("확인", new OnTextSetListener() {
            @Override
            public void onTextSet(WheelViewDialog wheelViewDialog, String text) {
                wheelViewDialog.dismiss();
                location = text;

                for (int i = 0; i < getResources().getStringArray(R.array.location).length; i++) {
                    if (getResources().getStringArray(R.array.location)[i].equals(text)) {
                        locationNum = i;
                    }
                }

                tvLocation.setText(location);
            }
        });
        locationDialog.setTitle("지역 선택");

        email = getIntent().getExtras().getString("email");
        pass = getIntent().getExtras().getString("pass");
        type = getIntent().getExtras().getString("type");

        try {
            edtName.setText(getIntent().getExtras().getString("nick"));
        } catch (NullPointerException e) {

        }

        layLocation_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivityForResult(new Intent(Join2Activity.this, LocationPopupActivity.class), LOCATION_SELECT_RESULT);
                locationDialog.show();
                overridePendingTransition(0, 0);
            }
        });
        layBirth_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                birthDialog.show();
//                startActivityForResult(new Intent(Join2Activity.this, BirthPopupActivity.class), BIRTH_SELECT_RESULT);
                overridePendingTransition(0, 0);
            }
        });

        layCheck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imvCheck1.setSelected(true);
                imvCheck2.setSelected(false);
                sex = "m";
            }
        });

        layCheck2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imvCheck1.setSelected(false);
                imvCheck2.setSelected(true);
                sex = "w";
            }
        });

        layNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }

    private void submit() {
        //TODO 이름, 지역, 날짜, 성별 체크후
        if (edtName.getText().toString().length() > 0) {
            if (!location.equals("")) {
                if (!birth.equals("")) {
                    if (imvCheck1.isSelected() || imvCheck2.isSelected()) {
                        if (imvCheck1.isSelected()) {
                            sex = "1";
                        } else if (imvCheck2.isSelected()) {
                            sex = "0";
                        } else {
                            sex = "1";
                        }

                        HashMap<String, String> params = new HashMap<>();
                        params.put("id", email);
                        params.put("pw", pass);
                        params.put("name", edtName.getText().toString());
                        params.put("sex", sex);
                        params.put("birth", birth);
                        params.put("confirm", "0");
                        params.put("loc", locationNum + "");
                        params.put("gcm", ConnectionController.PUSH_TOKEN);
                        params.put("type", type);

                        new APITaskNew(Join2Activity.this, params, ConnectionController.API_JOIN, joinResult).execute();

                    } else {
                        ConnectionController.getToast(getApplicationContext(), "성별을 선택해주세요.");
                        laySex.startAnimation(ConnectionController.shake);
                    }
                } else {
                    ConnectionController.getToast(getApplicationContext(), "출생연도를 선택해주세요.");
                    layBirth_.startAnimation(ConnectionController.shake);
                }
            } else {
                ConnectionController.getToast(getApplicationContext(), "지역을 선택해주세요.");
                layLocation_.startAnimation(ConnectionController.shake);
            }
        } else {
            edtName.startAnimation(ConnectionController.shake);
            ConnectionController.getToast(getApplicationContext(), "이름을 입력해주세요.");
        }
    }

    private StringTransMethod joinResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getString("result").equals("0")) {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("id", email);
                    params.put("pw", pass);
                    params.put("gcm", ConnectionController.PUSH_TOKEN);
                    params.put("type", type);

                    new APITaskNew(Join2Activity.this, params, ConnectionController.API_LOGIN, loginResult).execute();
                } else {
                    ConnectionController.getToast(getApplicationContext(), getResources().getString(R.string.warning_api_error));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private StringTransMethod loginResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            Log.e("re2", result);
            try {
                JSONObject jd = new JSONObject(result);

                ConnectionController.se.putBoolean("isLogin", true);
                ConnectionController.se.putInt("mIdx", Integer.parseInt(jd.getString("mIdx")));
                ConnectionController.se.putString("mId", jd.getString("mId"));
                ConnectionController.se.putString("mPw", pass);
                ConnectionController.se.putString("mName", jd.getString("mName"));
                ConnectionController.se.putInt("mLoc", Integer.parseInt(jd.getString("mLoc")));
                ConnectionController.se.putInt("mBirth", Integer.parseInt(jd.getString("mBirth")));
                ConnectionController.se.putInt("mSex", Integer.parseInt(jd.getString("mSex")));
                ConnectionController.se.putBoolean("mConfirm", Integer.parseInt(jd.getString("mConfirm")) == 0 ? false : true);
                ConnectionController.se.putBoolean("mBlock", Integer.parseInt(jd.getString("mBlock")) == 0 ? false : true);
                ConnectionController.se.putString("mBlockDate", jd.getString("mBlockDate"));
                ConnectionController.se.putString("mBlockReason", jd.getString("mBlockReason"));
                ConnectionController.se.putInt("mAntenna", Integer.parseInt(jd.getString("mAntenna")));
                ConnectionController.se.putInt("mJoinType", Integer.parseInt(jd.getString("mJoinType")));
                ConnectionController.se.putString("mLoveType", jd.getString("mLoveType"));
                ConnectionController.se.putString("mRegDate", jd.getString("mRegDate"));
                ConnectionController.se.commit();

                startActivity(new Intent(Join2Activity.this, LoveTendencyTestIntroActivity.class));
                overridePendingTransition(R.anim.fade, R.anim.hold);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (type.equals("10001")) {
            startActivity(new Intent(Join2Activity.this, Join1Activity.class));
            finish();
        } else {
            startActivity(new Intent(Join2Activity.this, LoginActivity.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SELECT_RESULT:
                if (resultCode == RESULT_OK) {
                    location = data.getExtras().getString("result");
                    locationNum = data.getExtras().getInt("resultNum");
                    tvLocation.setText(location);
                }
                break;
            case BIRTH_SELECT_RESULT:
                if (resultCode == RESULT_OK) {
                    birth = data.getExtras().getString("result");
                    tvBirth.setText(birth);
                }
                break;
        }
    }

}
