package com.tambourine.devmin.connection.utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jong-min on 2015-10-08.
 */
public class APITaskNew extends AsyncTask<Void, Void, Void> {

    Map<String, String> params;
    int serviceCode = 0;
    StringTransMethod stm;
    Context con;
    JSONObject timeOutResult;
    JSONObject nonNetworkResult;

    String return_msg = "";
    private String param = "abcdeabcdeabcde";

    public APITaskNew(Context context, Map<String, String> inputParam, int code, StringTransMethod stmm) {

        timeOutResult = new JSONObject();
        try {
            timeOutResult.put("result", 100);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nonNetworkResult = new JSONObject();
        try {
            nonNetworkResult.put("result", 1000);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.con = context;

        this.params = inputParam;
        this.serviceCode = code;
        this.stm = stmm;

        if (ConnectionController.NETWORK_STATUS == 0) {
            Handler handler = new Handler(ConnectionController.context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ConnectionController.context, ConnectionController.context.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();

                    stm.set_result(return_msg);
                }
            });
            return_msg = nonNetworkResult.toString();
            return;
        } else if (ConnectionController.NETWORK_STATUS == NetworkUtil.TYPE_3G) {
            Handler handler = new Handler(ConnectionController.context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ConnectionController.context, ConnectionController.context.getResources().getString(R.string.warning_3g), Toast.LENGTH_SHORT).show();
                }
            });
        }

        Iterator<String> iter = params.keySet().iterator();
        if (params.size() == 0) {
            param = "";
        } else {
            param = "?";
            String key = null, value = null;
            while (iter.hasNext()) {
                key = iter.next();
                value = params.get(key);

                param += key + "=" + value + "&";

                Log.e("param", key + " / " + value);

//          inputJO.put(key, value);
            }

            param = param.substring(0, param.length() - 1);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {

            return_msg = HttpRequest.post(ConnectionController.getAPIUrl(con, serviceCode)).form(this.params).body();

//            return_msg = HttpRequest.get(ConnectionController.getAPIUrl(serviceCode)).code() + "";
//            return_msg = new String(baos.toByteArray());

        } catch (Exception e) {
            Log.e("TCP", e.toString());
            return_msg = "";
        } finally {
            try {
                if (!return_msg.equals("")) {
                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stm.set_result(return_msg);
                        }
                    });
                }
            } catch (NullPointerException e) {
                ((Activity) con).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stm.set_result(timeOutResult.toString());
                    }
                });
            }
        }

        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

//    @Override
//    protected void onPostExecute(Void result) {
//        try{
//            if (!return_msg.equals("")) {
//                stm.set_result(return_msg);
//            }
//        }catch (NullPointerException e){
//            stm.set_result(timeOutResult.toString());
//        }
//        super.onPostExecute(result);
//    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }
}
