package com.tambourine.devmin.connection.items;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.AntennaListItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class AntennaListUIItem extends AbstractItem<AntennaListUIItem, AntennaListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public AntennaListItem item;

    public AntennaListUIItem withItem(AntennaListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.antenna_list_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_antenna_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        holder.tvDate.setText(item.getDate());

        switch (item.getType()) {
            case 0:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowlove);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment1));
                holder.tvCount.setTextColor(0xffffffff);
                holder.tvCount.setText("-" + item.getCount() + "개");
                break;
            case 1:
                holder.imvIcon.setImageResource(R.drawable.icon_chat_yellow);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment2));
                holder.tvCount.setTextColor(0xffffffff);
                holder.tvCount.setText("-" + item.getCount() + "개");
                break;
            case 2:
                holder.imvIcon.setImageResource(R.drawable.icon_slidemenu_cash);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment3));
                holder.tvCount.setTextColor(0xfffed514);
                holder.tvCount.setText("+" + item.getCount() + "개");
                break;
            case 3:
                holder.imvIcon.setImageResource(R.drawable.icon_video_free_1);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment4));
                holder.tvCount.setTextColor(0xfffed514);
                holder.tvCount.setText("+" + item.getCount() + "개");
                break;
            case 4:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowspeaker);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment5));
                holder.tvCount.setTextColor(0xfffed514);
                holder.tvCount.setText("+" + item.getCount() + "개");
                break;
            case 5:
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_graylove);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment6));
                holder.tvCount.setTextColor(0xfffed514);
                holder.tvCount.setText("+" + item.getCount() + "개");
                break;
            case 6:
                holder.imvIcon.setImageResource(R.drawable.icon_chat_gray);
                holder.tvMent.setText(con.getResources().getString(R.string.row_antenna_list_ment7));
                holder.tvCount.setTextColor(0xfffed514);
                holder.tvCount.setText("+" + item.getCount() + "개");
                break;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.imv_row_antenna_list_icon)
        protected ImageView imvIcon;
        @BindView(R.id.tv_row_antenna_list_date)
        protected TextView tvDate;
        @BindView(R.id.tv_row_antenna_list_ment)
        protected TextView tvMent;
        @BindView(R.id.tv_row_antenna_list_count)
        protected TextView tvCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
