package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class AntennaListItem {

    private String id = "";
    private String date = "";
    private int type = 0;
    private int count = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
