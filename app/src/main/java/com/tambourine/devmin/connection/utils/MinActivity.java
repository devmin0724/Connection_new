package com.tambourine.devmin.connection.utils;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by devmin on 2016-12-01.
 */

public class MinActivity extends Activity {

    @Override
    public void setContentView(int viewId) {
        View view = LayoutInflater.from(this).inflate(viewId, null);
        ViewGroup group = (ViewGroup)view;
        int childCnt = group.getChildCount();
        for(int i=0; i<childCnt; i++){
            View v = group.getChildAt(i);
            if(v instanceof TextView){
                ((TextView)v).setTypeface(ConnectionController.typeface);
            }else if(v instanceof EditText){
                ((EditText)v).setTypeface(ConnectionController.typeface);
            }
        }
        super.setContentView(view);
    }
}
