package com.tambourine.devmin.connection.items;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.ConnectionDetailActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.ConnectionListItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class ConnectionListUIItem extends AbstractItem<ConnectionListUIItem, ConnectionListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public ConnectionListItem item;

    public ConnectionListUIItem withItem(ConnectionListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.connection_list_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_connection_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        if (item.getSex() == 0) {
            holder.imvIcon.setImageResource(R.drawable.icon_main_woman);
            holder.tvSex.setText("여성");
            holder.tvSex.setTextColor(con.getResources().getColor(R.color.connection_find_list_woman_icon));
        } else {
            holder.imvIcon.setImageResource(R.drawable.icon_main_man);
            holder.tvSex.setText("남성");
            holder.tvSex.setTextColor(con.getResources().getColor(R.color.connection_find_list_man_icon));
        }

        holder.tvAge.setText(item.getAge() + "세");
        holder.tvLocation.setText(con.getResources().getStringArray(R.array.location)[item.getLocation()]);


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(con, ConnectionDetailActivity.class);
                i.putExtra("id", item.getId());
                con.startActivity(i);
                ((Activity) con).overridePendingTransition(0, 0);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.imv_row_connection_list_sex)
        protected ImageView imvIcon;
        @BindView(R.id.tv_row_connection_list_sex)
        protected TextView tvSex;
        @BindView(R.id.tv_row_connection_list_age)
        protected TextView tvAge;
        @BindView(R.id.tv_row_connection_list_location)
        protected TextView tvLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
