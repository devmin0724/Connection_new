package com.tambourine.devmin.connection.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.ConnectionListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.DividerItemDecoration;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.ConnectionListItem;
import com.tambourine.devmin.connection.widget.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by devmin on 2016-11-28.
 */

public class ConnectionListFragment extends Fragment {

    RecyclerViewEmptySupport rv;

    LinearLayoutManager layoutManager;
    FastItemAdapter<ConnectionListUIItem> adapter;

    int page = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_connection_list, container, false);
        rv = (RecyclerViewEmptySupport) rootView.findViewById(R.id.rv_connection_list);

        adapter = new FastItemAdapter<>();

        layoutManager = new LinearLayoutManager(getContext());

        rv.setLayoutManager(layoutManager);

        rv.setEmptyView(rootView.findViewById(R.id.tv_connection_list_empty_content));

        rv.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        rv.setAdapter(adapter);

        loadData();

        ConnectionController.setGlobalFont(rootView);
        return rootView;

    }

    private void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("page", page + "");

        new APITaskNew(getActivity(), params, ConnectionController.API_CONNECTION_LIST, clResult).execute();
    }

    private StringTransMethod clResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                Calendar _c = Calendar.getInstance();
                int year = _c.get(Calendar.YEAR);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    ConnectionListItem item = new ConnectionListItem();
                    item.setSex(Integer.parseInt(jd.getString("mSex")));
                    item.setId(jd.getString("mIdx"));
                    item.setAge(year - Integer.parseInt(jd.getString("mBirth")) + 1);
                    item.setLocation(Integer.parseInt(jd.getString("mLoc")));

                    adapter.add(new ConnectionListUIItem().withItem(item));
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }
                });
            }

        }
    };

//    private void loadData() {
//        Calendar _c = Calendar.getInstance();
//        int birth = (_c.get(Calendar.YEAR) + 1) - ConnectionController.sp.getInt("birth", 1986);
//        int location = ConnectionController.sp.getInt("location", 0);
//        for (int i = 0; i < 20; i++) {
//            ConnectionListItem item = new ConnectionListItem();
//            item.setSex(ConnectionController.sp.getInt("sex", 1) == 1 ? 0 : 1);
//            item.setId(i + "");
//            if (i <= 5) {
//                item.setAge(birth);
//                item.setLocation(0);
//            } else if (i > 5 && i <= 10) {
//                item.setAge(birth - 1);
//                item.setLocation(1);
//            } else if (i > 10 && i <= 15) {
//                item.setAge(birth + 1);
//                item.setLocation(7);
//            } else if (i > 15 && i <= 20) {
//                item.setAge(birth - 2);
//                item.setLocation(8);
//            } else if (i > 20 && i <= 25) {
//                item.setAge(birth + 2);
//                item.setLocation(3);
//            } else if (i > 25 && i <= 30) {
//                item.setAge(birth - 3);
//                item.setLocation(9);
//            } else if (i > 30 && i <= 35) {
//                item.setAge(birth + 3);
//                item.setLocation(10);
//            } else if (i > 35 && i <= 40) {
//                item.setAge(birth - 4);
//                item.setLocation(5);
//            } else if (i > 40 && i <= 45) {
//                item.setAge(birth + 4);
//                item.setLocation(11);
//            } else {
//                item.setAge(birth - 5);
//                item.setLocation(12);
//            }
//
//            adapter.add(new ConnectionListUIItem().withItem(item));
//        }
//
//        adapter.notifyDataSetChanged();
//    }
}
