package com.tambourine.devmin.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tambourine.devmin.connection.utils.APITaskNonActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by devmin on 2016-10-27.
 */

public class InstallReceiver extends BroadcastReceiver {
    Context con;
    String referrerString = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        con = context;
        if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
            Bundle extras = intent.getExtras();
            referrerString = extras.getString("referrer");
            Log.d("Referrer", "REFERRER: " + referrerString);

//            getDeviceSerialNumber();
        }
    }

    private String getDeviceSerialNumber() {
        final TelephonyManager tm = (TelephonyManager) con.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(con.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        ConnectionController.DID = deviceUuid.toString();

        ConnectionController.PUSH_TOKEN = FirebaseInstanceId.getInstance().getToken();

//        HashMap<String, String> params = new HashMap<>();
//        params.put("DID", ConnectionController.DID);
//        params.put("GCM", ConnectionController.PUSH_TOKEN);
//        params.put("referrer", referrerString);
//
//        new APITaskNonActivity(con, params, ConnectionController.API_REFERRER, referrerResult).execute();

        return ConnectionController.DID;
    }

    StringTransMethod referrerResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            Log.e("referrer", "referrer OK / DID : " + ConnectionController.DID + " / GCM : " + ConnectionController.PUSH_TOKEN + " / referrer : " + referrerString);
        }
    };
}
