package com.tambourine.devmin.connection.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by devmin on 2016-11-24.
 */

public class TypeWriterTextView extends EditText {
    private boolean isRunning = false;
    private long mTypeSpeed = 80;
    private long mDeleteSpeed = 50;
    private long mPauseDelay = 500;

    private Queue<Repeater> mRunnableQueue = new LinkedList<>();

    private Runnable mRunNextRunnable = new Runnable() {
        @Override
        public void run() {
            runNext();
        }
    };

    public TypeWriterTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setBackgroundColor(Color.TRANSPARENT);
        setCursorAtEnd();
        setCursorVisible(true);
        setRawInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
//        setType(context);
    }

    public TypeWriterTextView type(CharSequence text, long speed) {
        mRunnableQueue.add(new TextAdder(text, speed, mRunNextRunnable));
        if (!isRunning) runNext();
        return this;
    }

    public TypeWriterTextView type(CharSequence text) {
        return type(text, mTypeSpeed);
    }

    public TypeWriterTextView delete(CharSequence text, long speed) {
        mRunnableQueue.add(new TextRemover(text, speed, mRunNextRunnable));

        if (!isRunning) runNext();

        return this;
    }

    public TypeWriterTextView delete(CharSequence text) {
        return delete(text, mDeleteSpeed);
    }

    public TypeWriterTextView pause(long millis) {
        mRunnableQueue.add(new TypePauser(millis, mRunNextRunnable));

        if (!isRunning) runNext();

        return this;
    }

    public TypeWriterTextView run(Runnable runnable) {
        mRunnableQueue.add(new TypeRunnable(runnable, mRunNextRunnable));

        if (!isRunning) runNext();
        return this;
    }

    public TypeWriterTextView pause() {
        return pause(mPauseDelay);
    }

    private void setCursorAtEnd() {
        setSelection(getText().length());
    }

    private void runNext() {
        isRunning = true;
        Repeater next = mRunnableQueue.poll();

        if (next == null) {
            isRunning = false;
            return;
        }

        next.run();
    }

    private abstract class Repeater implements Runnable {
        protected Handler mHandler = new Handler();
        private Runnable mDoneRunnable;
        private long mDelay;

        public Repeater(Runnable doneRunnable, long delay) {
            mDoneRunnable = doneRunnable;
            mDelay = delay;
        }

        protected void done() {
            mDoneRunnable.run();
        }

        protected void delayAndRepeat() {
            mHandler.postDelayed(this, mDelay);
        }
    }

    private class TextAdder extends Repeater {

        private CharSequence mTextToAdd;

        public TextAdder(CharSequence textToAdd, long speed, Runnable doneRunnable) {
            super(doneRunnable, speed);

            mTextToAdd = textToAdd;
        }

        @Override
        public void run() {
            if (mTextToAdd.length() == 0) {
                done();
                return;
            }

            char first = mTextToAdd.charAt(0);
            mTextToAdd = mTextToAdd.subSequence(1, mTextToAdd.length());

            CharSequence text = getText();
            setText(text.toString() + first);
            setCursorAtEnd();
            delayAndRepeat();
        }
    }

    private class TextRemover extends Repeater {

        private CharSequence mTextToRemove;

        public TextRemover(CharSequence textToRemove, long speed, Runnable doneRunnable) {
            super(doneRunnable, speed);

            mTextToRemove = textToRemove;
        }

        @Override
        public void run() {
            if (mTextToRemove.length() == 0) {
                done();
                return;
            }

            char last = mTextToRemove.charAt(mTextToRemove.length() - 1);
            mTextToRemove = mTextToRemove.subSequence(0, mTextToRemove.length() - 1);

            CharSequence text = getText();

            if (text.charAt(text.length() - 1) == last) {
                setText(text.subSequence(0, text.length() - 1));
            }

            setCursorAtEnd();
            delayAndRepeat();
        }
    }

    private class TypePauser extends Repeater {

        boolean hasPaused = false;

        public TypePauser(long delay, Runnable doneRunnable) {
            super(doneRunnable, delay);
        }

        @Override
        public void run() {
            if (hasPaused) {
                done();
                return;
            }

            hasPaused = true;
            delayAndRepeat();
        }
    }

    private class TypeRunnable extends Repeater {

        Runnable mRunnable;

        public TypeRunnable(Runnable runnable, Runnable doneRunnable) {
            super(doneRunnable, 0);

            mRunnable = runnable;
        }

        @Override
        public void run() {
            mRunnable.run();
            done();
        }
    }

//    private void setType(Context context) {
//        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "DungGeunMo.otf"));
//    }
}