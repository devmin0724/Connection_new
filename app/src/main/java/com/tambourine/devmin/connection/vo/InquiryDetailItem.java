package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class InquiryDetailItem {
    private String id = "";
    private boolean isMe = false;
    private String ment = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean me) {
        isMe = me;
    }

    public String getMent() {
        return ment;
    }

    public void setMent(String ment) {
        this.ment = ment;
    }
}
