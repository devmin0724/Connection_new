package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class NotificationListItem {
    private int nId = 0;
    private int mId = 0;
    private int nType = 0;
    private int nPoint = 0;
    private int nSender = 0;
    private int nTarget = 0;
    private String nStr = "";
    private int dclId = 0;
    private boolean nIsView = false;
    private String nRegDate = "";

    public int getnId() {
        return nId;
    }

    public void setnId(int nId) {
        this.nId = nId;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getnType() {
        return nType;
    }

    public void setnType(int nType) {
        this.nType = nType;
    }

    public int getnPoint() {
        return nPoint;
    }

    public void setnPoint(int nPoint) {
        this.nPoint = nPoint;
    }

    public int getnSender() {
        return nSender;
    }

    public void setnSender(int nSender) {
        this.nSender = nSender;
    }

    public int getnTarget() {
        return nTarget;
    }

    public void setnTarget(int nTarget) {
        this.nTarget = nTarget;
    }

    public String getnStr() {
        return nStr;
    }

    public void setnStr(String nStr) {
        this.nStr = nStr;
    }

    public int getDclId() {
        return dclId;
    }

    public boolean isnIsView() {
        return nIsView;
    }

    public void setnIsView(boolean nIsView) {
        this.nIsView = nIsView;
    }

    public void setDclId(int dclId) {
        this.dclId = dclId;
    }

    public String getnRegDate() {
        return nRegDate;
    }

    public void setnRegDate(String nRegDate) {
        this.nRegDate = nRegDate;
    }
}
