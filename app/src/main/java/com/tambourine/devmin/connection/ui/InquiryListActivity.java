package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.InquiryListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.InquiryListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class InquiryListActivity extends MinCompatActivity {

    RecyclerView rv;
    LinearLayout layInquiry;
    FastItemAdapter<InquiryListUIItem> adapter;

    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_list);

        rv = (RecyclerView) findViewById(R.id.rv_inquiry_list);
        layInquiry = (LinearLayout) findViewById(R.id.lay_inquiry_list_inquiry);

        adapter = new FastItemAdapter<>();

        rv.setLayoutManager(new LinearLayoutManager(this));

        rv.setAdapter(adapter);

        layInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(InquiryListActivity.this, InquiryDetailActivity.class));
                overridePendingTransition(0, 0);
            }
        });

        loadData();

    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("page", page + "");

        new APITaskNew(InquiryListActivity.this, params, ConnectionController.API_INQUIRY_LIST, ilResult).execute();

    }

    private StringTransMethod ilResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    InquiryListItem item = new InquiryListItem();
                    item.setId(jd.getString("ilId"));
                    item.setQ(jd.getString("ilLastStr"));

                    adapter.add(new InquiryListUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };
}
