package com.tambourine.devmin.connection.utils;

import android.util.Log;

/**
 * Created by Jong-min on 2015-09-01.
 */
public class StringTransMethod {
    String _result = "";

    public String get_result() {
        return _result;
    }

    public void set_result(String result) {
        this._result = ConnectionController.decrypt(result, ConnectionController.SECRETKEY);
        Log.e("result1", result);
        Log.e("result2", this._result);

        endTrans(this._result);
    }

    public void endTrans(String result) {

    }
}
