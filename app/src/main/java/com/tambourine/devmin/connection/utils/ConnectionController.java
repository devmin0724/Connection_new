package com.tambourine.devmin.connection.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.*;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.ConnectionDetailActivity;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.log.DeviceLog;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.ads.misc.Utilities;
import com.unity3d.ads.webview.WebView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//import org.apache.commons.codec.binary.Base64;

/**
 * Created by devmin on 2016-09-09.
 */
public class ConnectionController extends MultiDexApplication {

    public static int NETWORK_STATUS = 1;
    public static Context context;
    public static SharedPreferences sp;
    public static SharedPreferences.Editor se;
    public static String PUSH_TOKEN;
    public static String DID;
    public static Animation shake;
    public static Activity currentActivity;
    public static String SECRETKEY;
    private static volatile ConnectionController instance = null;

    public static ArrayList<String> births;
    public static ArrayList<String> locations;
    public static List<String> loc;

    public static final String TYPEFACE_NAME = "DungGeunMo.otf";
    public static Typeface typeface = null;
    public static String incentivizedPlacementId = "";
    public static UnityAdsListener unityAdsListener = new UnityAdsListener();

    public static String tempTargetId, tempDlId;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(ConnectionController.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        instance = this;
        MultiDex.install(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        se = sp.edit();
        SECRETKEY = getResources().getString(R.string.sk);
        PUSH_TOKEN = FirebaseInstanceId.getInstance().getToken();

        KakaoSDK.init(new KakaoSDKAdapter());

        Calendar _c = Calendar.getInstance();
        int year = _c.get(Calendar.YEAR);

        births = new ArrayList<>();

        for (int i = year - 20; i >= year - 100; i--) {
            births.add(i + "");
        }

        locations = new ArrayList<>();

        for (int i = 0; i < getResources().getStringArray(R.array.location).length; i++) {
            locations.add(getResources().getStringArray(R.array.location)[i]);
        }

        loc = Arrays.asList(getResources().getStringArray(R.array.location));

        updateIconBadgeCount(getApplicationContext(), 0);

        loadTypeface();

//        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.ad_app_id));
    }

    private void loadTypeface() {
        if (typeface == null)
            typeface = Typeface.createFromAsset(getAssets(), TYPEFACE_NAME);
    }

    public static final int API_GCM = 0;
    public static final int API_VERSION_CHECK = 1;
    public static final int API_LOGIN = 2;
    public static final int API_EMAIL_CHECK = 3;
    public static final int API_JOIN = 4;
    public static final int API_CONFIRM = 5;
    public static final int API_FIND_PW = 6;
    public static final int API_LOVE_TEST = 7;
    public static final int API_LOVE_TEST_RESULT = 8;
    public static final int API_EDITINFO = 9;
    public static final int API_NOTIFICATION_LIST = 10;
    public static final int API_CONNECTION_FIND_LIST = 11;
    public static final int API_CONNECTION_LIST = 12;
    public static final int API_GET_USER_INFO = 13;
    public static final int API_GET_CONNECTION_DETAIL = 14;
    public static final int API_GET_ANTENNA_LIST = 15;
    public static final int API_GET_CHATTING_LIST = 16;
    public static final int API_GET_CHATTING_DETAIL = 17;
    public static final int API_MODIFY_INFO = 18;
    public static final int API_INQUIRY_LIST = 19;
    public static final int API_INQUIRY_LIST_DETAIL = 20;
    public static final int API_DATE_RESULT = 21;
    public static final int API_CHECK_JOIN = 22;
    public static final int API_DATE_LIST = 23;
    public static final int API_ASKING_FOR_DATE = 24;
    public static final int API_DATE_ACCESS = 25;
    public static final int API_DATING = 99;
    public static final int API_SIGN_OUT = 101;


    public static String getAPIUrl(Context con, int code) {
        String root = con.getString(R.string.API_ROOT);
        switch (code) {
            case API_GCM:
                return root + con.getString(R.string.API_VERSION_CHECK);
            case API_VERSION_CHECK:
                return root + con.getString(R.string.API_VERSION_CHECK);
            case API_LOGIN:
                return root + con.getString(R.string.API_LOGIN);
            case API_EMAIL_CHECK:
                return root + con.getString(R.string.API_EMAIL_CHECK);
            case API_JOIN:
                return root + con.getString(R.string.API_JOIN);
            case API_CONFIRM:
                return root + con.getString(R.string.API_CONFIRM);
            case API_FIND_PW:
                return root + con.getString(R.string.API_FIND_PW);
            case API_LOVE_TEST:
                return root + con.getString(R.string.API_LOVE_TEST);
            case API_LOVE_TEST_RESULT:
                return root + con.getString(R.string.API_LOVE_TEST_RESULT);
            case API_EDITINFO:
                return root + con.getString(R.string.API_EDITINFO);
            case API_DATING:
                return root + con.getString(R.string.API_DATING);
            case API_NOTIFICATION_LIST:
                return root + con.getString(R.string.API_NOTIFICATION_LIST);
            case API_CONNECTION_FIND_LIST:
                return root + con.getString(R.string.API_CONNECTION_FIND_LIST);
            case API_CONNECTION_LIST:
                return root + con.getString(R.string.API_CONNECTION_LIST);
            case API_GET_USER_INFO:
                return root + con.getString(R.string.API_GET_USER_INFO);
            case API_GET_CONNECTION_DETAIL:
                return root + con.getString(R.string.API_GET_CONNECTION_DETAIL);
            case API_GET_ANTENNA_LIST:
                return root + con.getString(R.string.API_GET_ANTENNA_LIST);
            case API_GET_CHATTING_LIST:
                return root + con.getString(R.string.API_GET_CHATTING_LIST);
            case API_GET_CHATTING_DETAIL:
                return root + con.getString(R.string.API_GET_CHATTING_DETAIL);
            case API_MODIFY_INFO:
                return root + con.getString(R.string.API_MODIFY_INFO);
            case API_INQUIRY_LIST:
                return root + con.getString(R.string.API_INQUIRY_LIST);
            case API_INQUIRY_LIST_DETAIL:
                return root + con.getString(R.string.API_INQUIRY_LIST_DETAIL);
            case API_DATE_RESULT:
                return root + con.getString(R.string.API_DATE_RESULT);
            case API_CHECK_JOIN:
                return root + con.getString(R.string.API_CHECK_JOIN);
            case API_DATE_LIST:
                return root + con.getString(R.string.API_DATE_LIST);
            case API_ASKING_FOR_DATE:
                return root + con.getString(R.string.API_ASKING_FOR_DATE);
            case API_DATE_ACCESS:
                return root + con.getString(R.string.API_DATE_ACCESS);
            case API_SIGN_OUT:
                return root + con.getString(R.string.API_SIGN_OUT);
            default:
                return null;
        }
    }

    public static void updateIconBadgeCount(Context context, int count) {
        Log.d("test", "updateIconBadgeCount");
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");

        // Component를 정의
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", getLauncherClassName(context));

        // 카운트를 넣어준다.
        intent.putExtra("badge_count", count);

        // Version이 3.1이상일 경우에는 Flags를 설정하여 준다.
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            intent.setFlags(0x00000020);
        }

        // send
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(context.getPackageName());

        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent, 0);
        if (resolveInfoList != null && resolveInfoList.size() > 0) {
            Log.d("test", "success");
            return resolveInfoList.get(0).activityInfo.name;

        }
        Log.d("test", "Fail");
        return "";
    }

    public static boolean checkEmail(String email) {

        if (email.length() > 45) {
            return false;
        }

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
//        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();

        return isNormal;
    }

    public static boolean checkPass(String pass) {
//        Pattern p = Pattern.compile("^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{6,15}$");
        Pattern p = Pattern.compile("^(?=.*[a-zA-Z]+)(?=.*[0-9]+).{6,15}$");
        Matcher m = p.matcher(pass);

        return m.matches();
    }

    public static void setGlobalFont(View view) {
        if (view != null) {
            if (view instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) view;
                int len = vg.getChildCount();
                for (int i = 0; i < len; i++) {
                    View v = vg.getChildAt(i);
                    if (v instanceof TextView) {
                        ((TextView) v).setTypeface(ConnectionController.typeface);
                    } else if (v instanceof EditText) {
                        ((EditText) v).setTypeface(ConnectionController.typeface);
                    }

                    setGlobalFont(v);
                }
            }
        } else {
            Log.e("setGlobalFont", "This is null  ");
        }

    }

    private static class KakaoSDKAdapter extends KakaoAdapter {
        /**
         * Session Config에 대해서는 default값들이 존재한다.
         * 필요한 상황에서만 override해서 사용하면 됨.
         *
         * @return Session의 설정값.
         */
        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[]{AuthType.KAKAO_LOGIN_ALL};
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public boolean isSecureMode() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSaveFormData() {
                    return true;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {
            return new IApplicationConfig() {
                @Override
                public Activity getTopActivity() {
                    return ConnectionController.getCurrentActivity();
                }

                @Override
                public Context getApplicationContext() {
                    return ConnectionController.getGlobalApplicationContext();
                }
            };
        }
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    // Activity가 올라올때마다 Activity의 onCreate에서 호출해줘야한다.
    public static void setCurrentActivity(Activity currentActivity) {
        ConnectionController.currentActivity = currentActivity;
    }

    public static ConnectionController getGlobalApplicationContext() {
        if (instance == null)
            throw new IllegalStateException("this application does not inherit com.kakao.GlobalApplication");
        return instance;
    }

    public static void getToast(Context con, String str) {
        Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    public static String encrypt(String input, String key) {
        byte[] crypted = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skey);
            crypted = cipher.doFinal(input.getBytes());
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return new String(android.util.Base64.encode(crypted, android.util.Base64.DEFAULT));
//        return new String(Base64.encodeBase64(crypted));
    }

    public static String decrypt(String input, String key) {
        byte[] output = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skey);
            output = cipher.doFinal(Base64.decode(input.getBytes(), Base64.DEFAULT));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return new String(output);
    }

    public static class UnityAdsListener implements IUnityAdsListener {

        @Override
        public void onUnityAdsReady(final String zoneId) {

            DeviceLog.debug("onUnityAdsReady: " + zoneId);
            Utilities.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // look for various default placement ids over time
                    switch (zoneId) {
                        case "video":
                        case "defaultZone":
                        case "defaultVideoAndPictureZone":
//                            interstitialPlacementId = zoneId;
//                            enableButton((Button) findViewById(R.id.unityads_example_interstitial_button));
                            break;

                        case "rewardedVideo":
                        case "rewardedVideoZone":
                        case "incentivizedZone":
                            ConnectionController.incentivizedPlacementId = zoneId;
//                            enableButton((Button) findViewById(R.id.unityads_example_incentivized_button));
                            break;
                    }
                }
            });

//            toast("Ready", zoneId);
        }

        @Override
        public void onUnityAdsStart(String zoneId) {
            DeviceLog.debug("onUnityAdsStart: " + zoneId);
//            toast("Start", zoneId);
        }

        @Override
        public void onUnityAdsFinish(String zoneId, UnityAds.FinishState result) {
            DeviceLog.debug("onUnityAdsFinish: " + zoneId + " - " + result);
//            toast("Finish", zoneId + " " + result);
            if(result == UnityAds.FinishState.COMPLETED){
                Log.e("unityAds", "COMPLETE");
                context.sendBroadcast(new Intent("com.tambourine.devmin.connection.AD_COMPLETE"));
            }
        }

        @Override
        public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
            DeviceLog.debug("onUnityAdsError: " + error + " - " + message);
//            toast("Error", error + " " + message);

        }

        private void toast(String callback, String msg) {
            Toast.makeText(context.getApplicationContext(), callback + ": " + msg, Toast.LENGTH_SHORT).show();
        }
    }

}
