package com.tambourine.devmin.connection.items;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.MainActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.DateListItem;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.PlayerMetaData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class DateListUIItem extends AbstractItem<DateListUIItem, DateListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public DateListItem item;

    //    DateBR mReceiver;
    private AlertDialog ad;

    public DateListUIItem withItem(DateListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.date_list;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_date_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        holder.tvTitle.setText(item.getDlTitle());
        holder.tvAntennaCount.setText(item.getDlPrice());
        holder.tvWriter.setText(item.getDlWriter());

        final AlertDialog.Builder buider = new AlertDialog.Builder(con);

        View view = (View) (((Activity) con).getLayoutInflater().
                inflate(R.layout.popup_date, null));

        TextView tvBody = (TextView) view.findViewById(R.id.tv_popup_date_body);
        tvBody.setText("데이트를 신청하면 안테나 " + item.getDlPrice() + "개가 사용대기 상태가 됩니다. 데이트신청을 하시겠습니까?");

        LinearLayout layAd = (LinearLayout) view.findViewById(R.id.lay_popup_date_ad);
        LinearLayout layYes = (LinearLayout) view.findViewById(R.id.lay_popup_date_yes);
        LinearLayout layNo = (LinearLayout) view.findViewById(R.id.lay_popup_date_no);
        layAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 광고 시청후 데이트 신청 ㄱㄱ
                PlayerMetaData playerMetaData = new PlayerMetaData(MainActivity.ac);
                playerMetaData.setServerId("com.tambourine.devmin.connection");
                playerMetaData.commit();

                MediationMetaData ordinalMetaData = new MediationMetaData(MainActivity.ac);
                ordinalMetaData.setOrdinal(1);
                ordinalMetaData.commit();

                if (UnityAds.isReady()) {
                    ConnectionController.tempTargetId = item.getTargetId();
                    ConnectionController.tempDlId = item.getDlId();
                    UnityAds.show(MainActivity.ac, ConnectionController.incentivizedPlacementId);
                } else {
//                    UnityAds.initialize(this, con.getResources().getString(R.string.unity_ads_game_id), unityAdsListener, false);
                }


            }
        });

        final StringTransMethod afdResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    if (jd.getString("result").equals("0")) {
                        ((Activity) con).finish();
                    } else if (jd.getString("result").equals("2")) {
                        ConnectionController.getToast(con.getApplicationContext(), "안테나가 부족합니다. 광고보고 데이트하기 혹은 구매샵에서 안테나를 구입해주세요.");
                    } else {
                        ConnectionController.getToast(con.getApplicationContext(), con.getResources().getString(R.string.warning_api_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        layYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO yes
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sender", ConnectionController.sp.getInt("mIdx", 1) + "");
                params.put("target", item.getTargetId());
                params.put("dlId", item.getDlId());
                params.put("ad", "0");

                new APITaskNew(con, params, ConnectionController.API_ASKING_FOR_DATE, afdResult).execute();
            }
        });

        layNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO no
                ad.dismiss();
            }
        });

        buider.setView(view);

        ad = buider.create();

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ad.show();
            }
        });

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.tv_row_date_list_title)
        protected TextView tvTitle;
        @BindView(R.id.tv_row_date_list_antenna_count)
        protected TextView tvAntennaCount;
        @BindView(R.id.tv_row_date_list_writer)
        protected TextView tvWriter;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
