package com.tambourine.devmin.connection.items;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.ChattingDetailActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.ChattingDetailListItem;
import com.tambourine.devmin.connection.vo.ConnectionListItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class ChattingListUIItem extends AbstractItem<ChattingListUIItem, ChattingListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public ChattingDetailListItem item;

    public ChattingListUIItem withItem(ChattingDetailListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.chatting_list_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_chatting_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        if (item.getSex() == 0) {
            holder.imvIcon.setImageResource(R.drawable.icon_main_woman);
            holder.tvSex.setText("여성");
            holder.tvSex.setTextColor(con.getResources().getColor(R.color.connection_find_list_woman_icon));
        } else {
            holder.imvIcon.setImageResource(R.drawable.icon_main_man);
            holder.tvSex.setText("남성");
            holder.tvSex.setTextColor(con.getResources().getColor(R.color.connection_find_list_man_icon));
        }

        holder.tvAge.setText(item.getBirth() + "세");
        holder.tvLocation.setText(con.getResources().getStringArray(R.array.location)[item.getLoc()]);


        switch (item.getCcIng()) {
            case 0:
                //TODO 신청중
                if (ConnectionController.sp.getInt("mIdx", 1) == item.getCcSendeer()) {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.tvBtn.setText("신청취소");
                    holder.tvBody.setText("채팅 신청 발송됨");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                } else if (ConnectionController.sp.getInt("mIdx", 1) == item.getCcTarget()) {
                    holder.layBtn.setVisibility(View.VISIBLE);
                    holder.layBtn2.setVisibility(View.VISIBLE);
                    holder.tvBtn.setText("수락");
                    holder.tvBtn2.setText("거절");
                    holder.tvBody.setText("채팅 신청 수신됨");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
                break;
            case 1:
                //TODO 중지됨
                if (ConnectionController.sp.getInt("mIdx", 1) == item.getCcSendeer()) {
                    holder.layBtn.setVisibility(View.GONE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.tvBody.setText("채팅 신청 거절됨");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                } else if (ConnectionController.sp.getInt("mIdx", 1) == item.getCcTarget()) {
                    holder.layBtn.setVisibility(View.GONE);
                    holder.layBtn2.setVisibility(View.GONE);
                    holder.tvBody.setText("채팅 신청 거절함");
                    holder.layBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
                break;
            case 2:
                //TODO 진행중
                holder.layBtn.setVisibility(View.VISIBLE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.tvBtn.setText("계속하기");
                holder.tvBody.setText("채팅 진행중");
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(con, ChattingDetailActivity.class);
                        i.putExtra("id", item.getCcId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(con, ChattingDetailActivity.class);
                        i.putExtra("id", item.getCcId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                    }
                });
                break;
            case 3:
                //TODO 종료
                holder.layBtn.setVisibility(View.GONE);
                holder.layBtn2.setVisibility(View.GONE);
                holder.tvBody.setText("채팅 종료됨");
                holder.layBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                holder.layBtn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                break;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.imv_row_chatting_list_sex)
        protected ImageView imvIcon;
        @BindView(R.id.tv_row_chatting_list_sex)
        protected TextView tvSex;
        @BindView(R.id.tv_row_chatting_list_age)
        protected TextView tvAge;
        @BindView(R.id.tv_row_chatting_list_location)
        protected TextView tvLocation;
        @BindView(R.id.tv_row_chatting_list_btn)
        protected TextView tvBtn;
        @BindView(R.id.tv_row_chatting_list_btn2)
        protected TextView tvBtn2;
        @BindView(R.id.tv_row_chatting_list_body)
        protected TextView tvBody;
        @BindView(R.id.lay_row_chatting_list_btn)
        protected LinearLayout layBtn;
        @BindView(R.id.lay_row_chatting_list_btn2)
        protected LinearLayout layBtn2;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
