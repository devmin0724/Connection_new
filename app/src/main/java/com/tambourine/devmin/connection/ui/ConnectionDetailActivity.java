package com.tambourine.devmin.connection.ui;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.ConnectionDetailUIItem;
import com.tambourine.devmin.connection.ui.popup.DateTypeSelectPopupActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.DateBR;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.ConnectionDetailItem;
import com.tambourine.devmin.connection.widget.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ConnectionDetailActivity extends MinCompatActivity {

    RecyclerViewEmptySupport rv;
    LinearLayout layNewDate;

    FastItemAdapter<ConnectionDetailUIItem> adapter;

    DateBR mReceiver;

    private String id = "";
    private int page = 1, tempPosition = 0;

    private AlertDialog.Builder adb;
    private AlertDialog ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_detail);

        rv = (RecyclerViewEmptySupport) findViewById(R.id.rv_connection_detail_list);
        layNewDate = (LinearLayout) findViewById(R.id.lay_connection_detail_new_date);

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            finish();
        }

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setEmptyView(findViewById(R.id.tv_connection_detail_list_empty_content));
        adapter = new FastItemAdapter<>();
        rv.setAdapter(adapter);

        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("com.tambourine.devmin.connection.DATE_PUSH");

        //동적 리시버 구현
        mReceiver = new DateBR() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                Intent i = new Intent(ConnectionDetailActivity.this, ConnectionDetailPopupActivity.class);
//                i.putExtra("id", id);
//                startActivity(i);
//                overridePendingTransition(0, 0);
            }

        };
        registerReceiver(mReceiver, intentfilter);

        layNewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 팝업 후 데이트 선택
                Intent i = new Intent(ConnectionDetailActivity.this, DateTypeSelectPopupActivity.class);
                i.putExtra("id", id);
                startActivity(i);
//                startActivity(new Intent(ConnectionDetailActivity.this, DateSelectActivity.class));
//                overridePendingTransition(0, 0);
//                PlayerMetaData playerMetaData = new PlayerMetaData(ConnectionDetailActivity.this);
//                playerMetaData.setServerId("com.tambourine.devmin.connection");
//                playerMetaData.commit();
//
//                MediationMetaData ordinalMetaData = new MediationMetaData(ConnectionDetailActivity.this);
//                ordinalMetaData.setOrdinal(1);
//                ordinalMetaData.commit();
//
//                UnityAds.show(ConnectionDetailActivity.this, incentivizedPlacementId);
            }
        });

        loadData();

    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("me", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("id", id);
        params.put("page", page + "");

        new APITaskNew(this, params, ConnectionController.API_GET_CONNECTION_DETAIL, cdResult).execute();

    }

    private StringTransMethod cdResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    ConnectionDetailItem item = new ConnectionDetailItem();
                    item.setSenderId(jd.getString("dclSender"));
                    item.setTargetId(jd.getString("dclTarget"));
                    item.setId(jd.getString("dclId"));
                    item.setDate(jd.getString("dclRegDate"));
                    item.setNowStatus(Integer.parseInt(jd.getString("dclIng")));
                    item.setDateName(jd.getString("dlTitle"));
                    item.setType(Integer.parseInt(jd.getString("dlType")));
                    item.setAntennaCount(Integer.parseInt(jd.getString("dclAntenna")));
                    item.setMComplete(jd.getString("dclMComplete").equals("1") ? true : false);
                    item.setWComplete(jd.getString("dclWComplete").equals("1") ? true : false);

                    adapter.add(new ConnectionDetailUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }
                });
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        adapter.clear();
        page = 1;
        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

}
