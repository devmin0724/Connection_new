package com.tambourine.devmin.connection.items;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.ConnectionDetailPopupItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class ConnectionDetailPopupListUIItem extends AbstractItem<ConnectionDetailPopupListUIItem, ConnectionDetailPopupListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public ConnectionDetailPopupItem item;

    public ConnectionDetailPopupListUIItem withItem(ConnectionDetailPopupItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.connection_detail_popup_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_connection_detail_popup_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        holder.tvQ.setText(item.getQ());
        if (item.isSex()) {
            //TODO 남자
            holder.tvWoman.setVisibility(View.GONE);
            holder.tvMan.setVisibility(View.VISIBLE);
            holder.tvMan.setText(item.getAnswer());
        } else {
            holder.tvWoman.setVisibility(View.VISIBLE);
            holder.tvMan.setVisibility(View.GONE);
            holder.tvWoman.setText(item.getAnswer());
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.tv_row_connection_detail_popup_q)
        protected TextView tvQ;
        @BindView(R.id.tv_row_connection_detail_popup_woman)
        protected TextView tvWoman;
        @BindView(R.id.tv_row_connection_detail_popup_man)
        protected TextView tvMan;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
