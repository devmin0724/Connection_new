package com.tambourine.devmin.connection.ui.popup;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinActivity;
import com.tambourine.devmin.connection.widget.WheelView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BirthPopupActivity extends MinActivity {
    String TAG = "log";

    @BindView(R.id.wv_birthpopup)
    WheelView wv;
    @BindView(R.id.lay_birth_popup_ok)
    LinearLayout layOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birth_popup);
        ButterKnife.bind(this);

        wv.setOffset(1);
        wv.setItems(ConnectionController.births);
        wv.setSeletion(10);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        layOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("result", wv.getSeletedItem());
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }

}
