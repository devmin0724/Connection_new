package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoveTendencyTestResultActivity extends MinCompatActivity {

    TextView tvResult1, tvResult2, tvResult3;
    LinearLayout layStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love_tendency_test_result);

        tvResult1 = (TextView) findViewById(R.id.tv_lttr_result_1);
        tvResult2 = (TextView) findViewById(R.id.tv_lttr_result_2);
        tvResult3 = (TextView) findViewById(R.id.tv_lttr_result_3);

        layStart = (LinearLayout) findViewById(R.id.lay_lttr_start);

        switch (getIntent().getExtras().getInt("result")) {
            case 101:
                tvResult1.setText("A");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[0]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[0]);
                break;
            case 102:
                tvResult1.setText("B");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[1]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[1]);
                break;
            case 103:
                tvResult1.setText("C");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[2]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[2]);
                break;
            case 104:
                tvResult1.setText("D");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[3]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[3]);
                break;
            case 105:
                tvResult1.setText("E");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[4]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[4]);
                break;
            case 106:
                tvResult1.setText("F");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[5]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[5]);
                break;
            case 107:
                tvResult1.setText("G");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[6]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[6]);
                break;
            case 108:
                tvResult1.setText("H");
                tvResult2.setText(getResources().getStringArray(R.array.date_type_test_result_title)[7]);
                tvResult3.setText(getResources().getStringArray(R.array.date_type_test_result)[7]);
                break;
        }

        layStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", ConnectionController.sp.getInt("mIdx", 0) + "");
                params.put("type", tvResult1.getText().toString());

                new APITaskNew(LoveTendencyTestResultActivity.this, params, ConnectionController.API_LOVE_TEST_RESULT, testResult).execute();
            }
        });

    }

    private StringTransMethod testResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getString("result").equals("0")) {
                    ConnectionController.se.putInt("loveTestResult", getIntent().getExtras().getInt("result"));
                    ConnectionController.se.commit();
                    startActivity(new Intent(LoveTendencyTestResultActivity.this, MainActivity.class));
                    finish();
                } else {
                    ConnectionController.getToast(getApplicationContext(), getResources().getString(R.string.warning_api_error));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
