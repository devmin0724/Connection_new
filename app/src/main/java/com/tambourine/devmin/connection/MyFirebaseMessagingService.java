package com.tambourine.devmin.connection;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tambourine.devmin.connection.ui.ChattingDetailActivity;
import com.tambourine.devmin.connection.ui.ChattingListActivity;
import com.tambourine.devmin.connection.ui.ConnectionDetailActivity;
import com.tambourine.devmin.connection.ui.DateActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;

import java.util.Iterator;
import java.util.Map;


/**
 * Created by devmin on 2016-09-09.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        Map<String, String> a = remoteMessage.getData();

        Iterator<String> iter = a.keySet().iterator();
        String key = null, value = null;
        while (iter.hasNext()) {
            key = iter.next();
            value = a.get(key);

            Log.e("param", key + " / " + value);

        }

        sendNotification(a.get("type"), a.get("id"));

//        sendBroadcast(new Intent("com.tambourine.devmin.villamoa.admin.INQUIRY_PUSH"));
    }

    private void sendNotification(String type, String id) {
        Intent i = null;
        String body = "", title = "";
        long[] vib = new long[]{1000, 1000, 1000, 1000, 1000};
        int smallIcon = 0;

        //TODO 1:데이트신청완료 2:데이트신청도착 3:데이트연결됨
        //TODO 4:데이트 거절됨 5:데이트 거절함 6:데이트종료됨
        //TODO 7:채팅신청완료 8:채팅신청도착 9:채팅 거절됨 10:채팅 거절함
        //TODO 11:채팅연결됨 12:데이트 취소함 13:데이트 취소됨
        //TODO 14:채팅 취소함 15:채팅 취소됨
        switch (Integer.parseInt(type)) {
            case 1:
                body = getResources().getString(R.string.notification_ment1);
                title = getResources().getString(R.string.notification_ment1_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_notice_yellowlove;
                break;
            case 2:
                body = getResources().getString(R.string.notification_ment2);
                title = getResources().getString(R.string.notification_ment2_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_notice_yellowlove;
                break;
            case 3:
                body = getResources().getString(R.string.notification_ment3);
                title = getResources().getString(R.string.notification_ment3_title);
                i = new Intent(this, DateActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_notice_pinklove;
                break;
            case 4:
                body = getResources().getString(R.string.notification_ment4);
                title = getResources().getString(R.string.notification_ment4_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_lovelist_graylove;
                break;
            case 5:
                body = getResources().getString(R.string.notification_ment5);
                title = getResources().getString(R.string.notification_ment5_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_lovelist_graylove;
                break;
            case 6:
                body = getResources().getString(R.string.notification_ment6);
                title = getResources().getString(R.string.notification_ment6_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_notice_pinklove;
                break;
            case 7:
                body = getResources().getString(R.string.notification_ment7);
                title = getResources().getString(R.string.notification_ment7_title);
                i = new Intent(this, ChattingListActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_yellow;
                break;
            case 8:
                body = getResources().getString(R.string.notification_ment8);
                title = getResources().getString(R.string.notification_ment8_title);
                i = new Intent(this, ChattingListActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_yellow;
                break;
            case 9:
                body = getResources().getString(R.string.notification_ment9);
                title = getResources().getString(R.string.notification_ment9_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_gray;
                break;
            case 10:
                body = getResources().getString(R.string.notification_ment10);
                title = getResources().getString(R.string.notification_ment10_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_gray;
                break;
            case 11:
                body = getResources().getString(R.string.notification_ment11);
                title = getResources().getString(R.string.notification_ment11_title);
                i = new Intent(this, ChattingDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_pink;
                break;
            case 12:
                body = getResources().getString(R.string.notification_ment12);
                title = getResources().getString(R.string.notification_ment12_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_lovelist_graylove;
                break;
            case 13:
                body = getResources().getString(R.string.notification_ment13);
                title = getResources().getString(R.string.notification_ment13_title);
                i = new Intent(this, ConnectionDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_lovelist_graylove;
                break;
            case 14:
                body = getResources().getString(R.string.notification_ment14);
                title = getResources().getString(R.string.notification_ment14_title);
                i = new Intent(this, ChattingDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_gray;
                break;
            case 15:
                body = getResources().getString(R.string.notification_ment15);
                title = getResources().getString(R.string.notification_ment15_title);
                i = new Intent(this, ChattingDetailActivity.class);
                i.putExtra("id", id);
                smallIcon = R.drawable.icon_chat_gray;
                break;
        }


        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i,
                PendingIntent.FLAG_ONE_SHOT);
        PendingIntent pi = PendingIntent.getActivity(this, 0, i,
                PendingIntent.FLAG_ONE_SHOT);

//        PendingIntent pIntent = PendingIntent.

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = null;

        switch (ConnectionController.sp.getInt("alramType", 0)) {
            case 0:
                //TODO 알림내용 + 알림음
                notificationBuilder = new NotificationCompat
                        .Builder(this)
                        .setSmallIcon(smallIcon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setTicker(title)
                        .setNumber(1)
//                        .addAction(R.drawable.icon_push_accept, "수락", pi)
//                        .addAction(R.drawable.icon_push_refuse, "거절", pi)
                        .setContentIntent(pendingIntent);
                break;
            case 1:
                //TODO 알림내용만
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(smallIcon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setTicker(title)
                        .setNumber(1)
                        .setContentIntent(pendingIntent);
                break;
            case 2:
                //TODO 알림음만
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(smallIcon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setNumber(1)
                        .setContentIntent(pendingIntent);
                break;
        }
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(notificationBuilder);
//        style.setSummaryText("더보기 +");
        style.setBigContentTitle(title);
        style.bigText(body);

        notificationBuilder.setStyle(style);

//        notificationBuilder.setOngoing(true).setUsesChronometer(true)
//                .setShowWhen(true);
//        notificationBuilder.setOngoing(true);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

}