package com.tambourine.devmin.connection.items;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.InquiryDetailActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.InquiryListItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class InquiryListUIItem extends AbstractItem<InquiryListUIItem, InquiryListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public InquiryListItem item;

    public InquiryListUIItem withItem(InquiryListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.inquiry_list_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_inquiry_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        holder.tvMent.setText(item.getQ());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(con, InquiryDetailActivity.class);
                //item.getId();
                i.putExtra("id", item.getId());
                con.startActivity(i);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.tv_row_inquiry_list_ment)
        protected TextView tvMent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
