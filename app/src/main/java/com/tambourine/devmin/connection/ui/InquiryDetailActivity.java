package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.InquiryDetailUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.InquiryDetailItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class InquiryDetailActivity extends MinCompatActivity {

    RecyclerView rv;
    EditText edt;
    TextView tv;
    FastItemAdapter<InquiryDetailUIItem> adapter;

    String id = "";
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_detail);

        rv = (RecyclerView) findViewById(R.id.rv_inquiry_detail);
        edt = (EditText) findViewById(R.id.edt_inquiry_detail);
        tv = (TextView) findViewById(R.id.tv_inquiry_detail);

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            id = "";
        }

        rv.setLayoutManager(new LinearLayoutManager(this));

        adapter = new FastItemAdapter<>();

        rv.setAdapter(adapter);

        InquiryDetailItem item = new InquiryDetailItem();
        item.setMe(false);
        item.setId("1");
        item.setMent("안녕하세요 접속 운영팀입니다. 무엇을 도와드릴까요?");

        adapter.add(new InquiryDetailUIItem().withItem(item));

        loadData();
    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        params.put("page", page + "");

        new APITaskNew(InquiryDetailActivity.this, params, ConnectionController.API_INQUIRY_LIST_DETAIL, ildResult).execute();
    }

    private StringTransMethod ildResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);

                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    InquiryDetailItem item = new InquiryDetailItem();
                    item.setMe(jd.getString("ildSender").equals("0") ? false : true);
                    item.setId(jd.getString("ildId"));
                    item.setMent(jd.getString("ildBody"));

                    adapter.add(new InquiryDetailUIItem().withItem(item));
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };
}
