package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.AntennaListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.AntennaListItem;
import com.tambourine.devmin.connection.widget.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AntennaListActivity extends MinCompatActivity {

    RecyclerViewEmptySupport rv;
    FastItemAdapter<AntennaListUIItem> adapter;
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_antenna_list);

        rv = (RecyclerViewEmptySupport) findViewById(R.id.rv_antenna_list);

        adapter = new FastItemAdapter<>();

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setEmptyView(findViewById(R.id.tv_antenna_list_empty_content));
        rv.setAdapter(adapter);

        loadData();
    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("page", page + "");

        new APITaskNew(this, params, ConnectionController.API_GET_ANTENNA_LIST, alResult).execute();

    }

    private StringTransMethod alResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    AntennaListItem item = new AntennaListItem();
                    item.setId(jd.getString("alId"));
                    item.setDate(jd.getString("alRegDate"));
                    item.setCount(Integer.parseInt(jd.getString("alPoint")));
                    item.setType(Integer.parseInt(jd.getString("alType")));

                    adapter.add(new AntennaListUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };

}
