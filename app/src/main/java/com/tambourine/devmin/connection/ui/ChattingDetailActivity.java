package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.InquiryDetailUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.InquiryDetailItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChattingDetailActivity extends MinCompatActivity {
    RecyclerView rv;
    EditText edt;
    TextView tv;
    FastItemAdapter<InquiryDetailUIItem> adapter;

    int page = 1;
    String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_detail);

        rv = (RecyclerView) findViewById(R.id.rv_chatting_detail);

        edt = (EditText) findViewById(R.id.edt_chatting_detail);
        tv = (TextView) findViewById(R.id.tv_chatting_detail);

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            finish();
        }

        rv.setLayoutManager(new LinearLayoutManager(this));

        adapter = new FastItemAdapter<>();

        rv.setAdapter(adapter);

        loadData();
    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        params.put("page", page + "");

        new APITaskNew(this, params, ConnectionController.API_GET_CHATTING_DETAIL, cdResult).execute();

//        InquiryDetailItem item = new InquiryDetailItem();
//        item.setMe(false);
//        item.setId("1");
//        item.setMent("안녕하세요.");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        item = new InquiryDetailItem();
//        item.setMe(true);
//        item.setId("1");
//        item.setMent("네 안녕하세요.");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        item = new InquiryDetailItem();
//        item.setMe(false);
//        item.setId("1");
//        item.setMent("데이트해보니 참 잘맞다 싶어서 채팅신청했는데 승인해주셔 감사해요 ㅎㅎㅎ");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        item = new InquiryDetailItem();
//        item.setMe(true);
//        item.setId("1");
//        item.setMent("앗 아니에요 저도 잘맞겠다 싶어서 신청하려했어요 ㅎㅎㅎㅎ");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        item = new InquiryDetailItem();
//        item.setMe(false);
//        item.setId("1");
//        item.setMent("앗 정말요 ㅎㅎㅎ 저희 카톡으로 이야기할까요? 제아이디에요 devLee");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        item = new InquiryDetailItem();
//        item.setMe(true);
//        item.setId("1");
//        item.setMent("아 넹 제가 등록하고 대화드릴게요!!");
//
//        adapter.add(new InquiryDetailUIItem().withItem(item));
//
//        adapter.notifyDataSetChanged();
    }

    private StringTransMethod cdResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);

                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    InquiryDetailItem item = new InquiryDetailItem();
                    item.setMe(jd.getString("cctSender").equals(ConnectionController.sp.getInt("mIdx", 1) + "") ? true : false);
                    item.setId("1");
                    item.setMent(jd.getString("cctBody"));

                    adapter.add(new InquiryDetailUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }

        }
    };
}
