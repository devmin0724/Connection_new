package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Join1Activity extends MinCompatActivity {

    EditText edtEmail, edtPass1, edtPass2;
    TextView tvTerm;
    ImageView imvTerm;
    LinearLayout layNext, layEmail, layPass1, layPass2, layTerm;

    boolean isPassMatch = false, isPassPatternMatch = false, isEmailPatternMatch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join1);

        edtEmail = (EditText) findViewById(R.id.edt_join_email);
        edtPass1 = (EditText) findViewById(R.id.edt_join_pass_1);
        edtPass2 = (EditText) findViewById(R.id.edt_join_pass_2);

        tvTerm = (TextView) findViewById(R.id.tv_join_term);

        layTerm = (LinearLayout) findViewById(R.id.lay_join_term);

        imvTerm = (ImageView) findViewById(R.id.imv_join_term);

        layNext = (LinearLayout) findViewById(R.id.lay_join_next);
        layEmail = (LinearLayout) findViewById(R.id.lay_join_email);
        layPass1 = (LinearLayout) findViewById(R.id.lay_join_pass_1);
        layPass2 = (LinearLayout) findViewById(R.id.lay_join_pass_2);

        try {
            edtEmail.setText(getIntent().getExtras().getString("email"));
            edtPass1.setText(getIntent().getExtras().getString("pass"));
            edtPass2.setText(getIntent().getExtras().getString("pass"));
        } catch (NullPointerException e) {

        }

        layTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imvTerm.isSelected()) {
//                    fillTheContents(false);
                    imvTerm.setSelected(false);
                } else {
//                    fillTheContents(true);
                    imvTerm.setSelected(true);
                }
            }
        });

        layNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String temp = charSequence.toString();
                if (temp.length() > 0) {
                    isEmailPatternMatch = ConnectionController.checkEmail(temp);
                } else {
                    isEmailPatternMatch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtPass1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String temp = charSequence.toString();
                if (temp.length() > 0) {
                    isPassPatternMatch = ConnectionController.checkPass(temp);
                } else {
                    isPassPatternMatch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtPass2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String temp = charSequence.toString();
                if (temp.length() > 0) {
                    isPassMatch = temp.equals(edtPass1.getText().toString());
                } else {
                    isPassMatch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void fillTheContents(boolean is) {
        if (is) {
            edtEmail.setText("a@a.a");
            edtPass1.setText("aaaaa1");
            edtPass2.setText("aaaaa1");
        } else {
            edtEmail.setText("");
            edtPass1.setText("");
            edtPass2.setText("");
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Join1Activity.this, LoginActivity.class));
        finish();
    }

    private void submit() {
        if (edtEmail.getText().toString().length() > 0) {
            if (edtPass1.getText().toString().length() > 0 && edtPass2.getText().toString().length() > 0) {
                if (isEmailPatternMatch) {
                    if (isPassMatch) {
                        if (isPassPatternMatch) {
                            if (imvTerm.isSelected()) {

                                HashMap<String, String> params = new HashMap<>();
                                params.put("email", edtEmail.getText().toString());

                                new APITaskNew(Join1Activity.this, params, ConnectionController.API_EMAIL_CHECK, emailCheckResult).execute();

                            } else {
                                layTerm.startAnimation(ConnectionController.shake);
                                ConnectionController.getToast(getApplicationContext(), "약관 동의가 필요합니다.");
                            }
                        } else {
                            layPass1.startAnimation(ConnectionController.shake);
                            layPass2.startAnimation(ConnectionController.shake);
                            ConnectionController.getToast(getApplicationContext(), "비밀번호는 영문 숫자 조합 6자리 이상으로 설정해주세요.");
                        }
                    } else {
                        layPass1.startAnimation(ConnectionController.shake);
                        layPass2.startAnimation(ConnectionController.shake);
                        ConnectionController.getToast(getApplicationContext(), "비밀번호가 일치하지 않습니다.");
                    }
                } else {
                    layEmail.startAnimation(ConnectionController.shake);
                    ConnectionController.getToast(getApplicationContext(), "이메일 형식이 아닙니다.");
                }
            } else {
                layPass1.startAnimation(ConnectionController.shake);
                layPass2.startAnimation(ConnectionController.shake);
                ConnectionController.getToast(getApplicationContext(), "비밀번호를 입력해주세요.");
            }
        } else {
            layEmail.startAnimation(ConnectionController.shake);
            ConnectionController.getToast(getApplicationContext(), "이메일을 입력해주세요.");
        }
    }

    private StringTransMethod emailCheckResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getString("result").equals("0")) {
                    Intent i = new Intent(Join1Activity.this, Join2Activity.class);
                    i.putExtra("email", edtEmail.getText().toString());
                    i.putExtra("pass", edtPass1.getText().toString());
                    i.putExtra("type", "10001");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (jd.getString("result").equals("1")) {
                    Toast.makeText(getApplicationContext(), "이미 가입된 아이디가 있습니다. 비밀번호 찾기를 이용해주세요.", Toast.LENGTH_SHORT).show();
                    layEmail.startAnimation(ConnectionController.shake);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_api_error), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
