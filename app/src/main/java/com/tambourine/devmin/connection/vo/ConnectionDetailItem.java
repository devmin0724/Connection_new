package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-02.
 */

public class ConnectionDetailItem {
    private String id = "";
    private int type = 0;
    private String date = "";
    private String dateName = "";
    private int nowStatus = 0;
    private String senderId = "";
    private String targetId = "";
    private int antennaCount = 0;
    private boolean isMComplete = false;
    private boolean isWComplete = false;

    public boolean isMComplete() {
        return isMComplete;
    }

    public void setMComplete(boolean MComplete) {
        isMComplete = MComplete;
    }

    public boolean isWComplete() {
        return isWComplete;
    }

    public void setWComplete(boolean WComplete) {
        isWComplete = WComplete;
    }

    public int getAntennaCount() {
        return antennaCount;
    }

    public void setAntennaCount(int antennaCount) {
        this.antennaCount = antennaCount;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateName() {
        return dateName;
    }

    public void setDateName(String dateName) {
        this.dateName = dateName;
    }

    public int getNowStatus() {
        return nowStatus;
    }

    public void setNowStatus(int nowStatus) {
        this.nowStatus = nowStatus;
    }
}
