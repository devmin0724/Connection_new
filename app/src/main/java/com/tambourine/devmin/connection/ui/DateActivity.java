package com.tambourine.devmin.connection.ui;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.popup.ConnectionDetailPopupActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DateActivity extends MinCompatActivity {

    ImageView imvX, imvChat, imvBG;
    TextView tvQ, tvA1, tvA2, tvA3, tvA4, tvA5;
    LinearLayout layA1, layA2, layA3, layA4, layA5;

    int type = 1;
    String imgName = "", id = "";

    //dateId=10&start=1&qn=1&an=1&sex=0
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        imvX = (ImageView) findViewById(R.id.imv_date_x);
        imvChat = (ImageView) findViewById(R.id.imv_date_chat);
        imvBG = (ImageView) findViewById(R.id.imv_date_bg);

        tvQ = (TextView) findViewById(R.id.tv_date_q);
        tvA1 = (TextView) findViewById(R.id.tv_date_a_1);
        tvA2 = (TextView) findViewById(R.id.tv_date_a_2);
        tvA3 = (TextView) findViewById(R.id.tv_date_a_3);
        tvA4 = (TextView) findViewById(R.id.tv_date_a_4);
        tvA5 = (TextView) findViewById(R.id.tv_date_a_5);

        layA1 = (LinearLayout) findViewById(R.id.lay_date_a_1);
        layA2 = (LinearLayout) findViewById(R.id.lay_date_a_2);
        layA3 = (LinearLayout) findViewById(R.id.lay_date_a_3);
        layA4 = (LinearLayout) findViewById(R.id.lay_date_a_4);
        layA5 = (LinearLayout) findViewById(R.id.lay_date_a_5);

        setupWindowAnimation();

        try {
            type = getIntent().getExtras().getInt("type");
        } catch (NullPointerException e) {
            type = 1;
        }

        try {
            imgName = getIntent().getExtras().getString("imgName");
        } catch (NullPointerException e) {
            imgName = "date1";
        }

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            id = "1";
        }

        imvX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        imvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DateActivity.this, ChattingDetailActivity.class));
                overridePendingTransition(0, 0);
                finish();
            }
        });

        dating(0);

    }

    private void setupWindowAnimation() {
        Transition fade = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            fade = TransitionInflater.from(this).inflateTransition(R.transition.activity_fade);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setReturnTransition(fade);
            getWindow().setExitTransition(fade);
            getWindow().setReenterTransition(fade);
            getWindow().setEnterTransition(fade);
        }
    }

    private void dating(int an) {
        HashMap<String, String> params = new HashMap<>();
        params.put("dclId", id + "");
        params.put("an", an + "");
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("sex", ConnectionController.sp.getInt("mSex", 0) + "");

        new APITaskNew(this, params, ConnectionController.API_DATING, datingResult).execute();

//        datingGo(TempDB.dating(type, is, qn, an, sex));

    }

    private StringTransMethod datingResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            datingGo(result);
        }
    };

    private void datingGo(String result) {
        try {
            final JSONObject jd = new JSONObject(result);

            if (jd.getString("result").equals("0")) {
                final String questionNumber = jd.getString("ddlQuestionNumber");
                setupWindowAnimation();
                Log.e("question Number", questionNumber);
                Log.e("img", "http://apis.tbr-app.co.kr/apis/connection/imgs/date" + jd.getString("dlId") + "_" + Integer.parseInt(questionNumber) + ".jpg");
                Glide.with(DateActivity.this)
                        .load("http://apis.tbr-app.co.kr/apis/connection/imgs/date" + jd.getString("dlId") + "_" + Integer.parseInt(questionNumber) + ".jpg")
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.no_image)
                        .centerCrop()
                        .animate(animationObject)
                        .thumbnail(0.1f)
                        .into(imvBG);
                switch (Integer.parseInt(jd.getString("ddlACount"))) {
                    case 1:
                        layA1.setVisibility(View.VISIBLE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);
                        break;
                    case 2:
                        layA1.setVisibility(View.VISIBLE);
                        layA2.setVisibility(View.VISIBLE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);
                        break;
                    case 3:
                        layA1.setVisibility(View.VISIBLE);
                        layA2.setVisibility(View.VISIBLE);
                        layA3.setVisibility(View.VISIBLE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);
                        break;
                    case 4:
                        layA1.setVisibility(View.VISIBLE);
                        layA2.setVisibility(View.VISIBLE);
                        layA3.setVisibility(View.VISIBLE);
                        layA4.setVisibility(View.VISIBLE);
                        layA5.setVisibility(View.GONE);
                        break;
                    default:
                        layA1.setVisibility(View.VISIBLE);
                        layA2.setVisibility(View.VISIBLE);
                        layA3.setVisibility(View.VISIBLE);
                        layA4.setVisibility(View.VISIBLE);
                        layA5.setVisibility(View.VISIBLE);
                        break;
                }
                tvA1.setText(jd.getString("ddlA1"));
                tvA2.setText(jd.getString("ddlA2"));
                tvA3.setText(jd.getString("ddlA3"));
                tvA4.setText(jd.getString("ddlA4"));
                tvA5.setText(jd.getString("ddlA5"));
                layA1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layA1.setVisibility(View.GONE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);

                        if (type == 0) {
                            tvQ.setText("상대방이 답변 선택중입니다..");
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    dating(1);
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 2000);
                        } else {
                            dating(1);
                        }

                    }
                });
                layA2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layA1.setVisibility(View.GONE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);

                        if (type == 0) {
                            tvQ.setText("상대방이 답변 선택중입니다..");
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    dating(2);
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 2000);
                        } else {
                            dating(2);
                        }
                    }
                });
                layA3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layA1.setVisibility(View.GONE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);

                        if (type == 0) {
                            tvQ.setText("상대방이 답변 선택중입니다..");
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    dating(3);
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 2000);
                        } else {
                            dating(3);
                        }
                    }
                });
                layA4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layA1.setVisibility(View.GONE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);

                        if (type == 0) {
                            tvQ.setText("상대방이 답변 선택중입니다..");
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    dating(4);
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 2000);
                        } else {
                            dating(4);
                        }
                    }
                });
                layA5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layA1.setVisibility(View.GONE);
                        layA2.setVisibility(View.GONE);
                        layA3.setVisibility(View.GONE);
                        layA4.setVisibility(View.GONE);
                        layA5.setVisibility(View.GONE);

                        if (type == 0) {
                            tvQ.setText("상대방이 답변 선택중입니다..");
                            Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    dating(5);
                                }
                            };
                            handler.sendEmptyMessageDelayed(0, 2000);
                        } else {
                            dating(5);
                        }
                    }
                });

                tvQ.setText(jd.getString("ddlQuestion"));

            } else if (jd.getString("result").equals("2")) {
                Intent i = new Intent(DateActivity.this, ConnectionDetailPopupActivity.class);
//                        i.putExtra("id", id);
                startActivity(i);
                overridePendingTransition(0, 0);
                Toast.makeText(getApplicationContext(), "데이트 종료", Toast.LENGTH_LONG).show();
                finish();
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ViewPropertyAnimation.Animator animationObject = new ViewPropertyAnimation.Animator() {
        @Override
        public void animate(View view) {
            // if it's a custom view class, cast it here
            // then find subviews and do the animations
            // here, we just use the entire view for the fade animation
            view.setAlpha(0f);

            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(500);
            fadeAnim.start();
        }
    };

}
