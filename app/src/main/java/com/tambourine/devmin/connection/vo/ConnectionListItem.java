package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-01.
 */

public class ConnectionListItem {

    public int sex = 0; //0:여성 1:남성
    public int age = 0;
    public int location = 0;
    public String id = "";

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
