package com.tambourine.devmin.connection.ui.popup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.DateSelectActivity;
import com.tambourine.devmin.connection.utils.MinActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DateTypeSelectPopupActivity extends MinActivity {

    @BindView(R.id.lay_date_type_select_1)
    LinearLayout lay1;
    @BindView(R.id.lay_date_type_select_2)
    LinearLayout lay2;

    private String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_type_select_popup);
        ButterKnife.bind(this);

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            finish();
        }

        final Intent i = new Intent(DateTypeSelectPopupActivity.this, DateSelectActivity.class);
        i.putExtra("id", id);

        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("type", 0);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
        });

        lay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("type", 1);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
        });
    }
}
