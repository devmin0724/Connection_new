package com.tambourine.devmin.connection.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.InitActivity;
import com.tambourine.devmin.connection.ui.InquiryListActivity;
import com.tambourine.devmin.connection.ui.MainActivity;
import com.tambourine.devmin.connection.ui.ModifyInfoActivity;
import com.tambourine.devmin.connection.ui.NotificationActivity;
import com.tambourine.devmin.connection.ui.TermsActivity;
import com.tambourine.devmin.connection.ui.popup.LogoutPopupActivity;
import com.tambourine.devmin.connection.ui.popup.SignOutPoopupActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by devmin on 2016-11-28.
 */

public class SettingsFragment extends Fragment {

    private final int LOGOUT_RETURN = 1111;
    private final int SIGNOUT_RETURN = 1112;

    LinearLayout layAlram1, layAlram2, layAlram3;
    ImageView imvAlram1, imvAlram2, imvAlram3;
    TextView tvNotification, tvModifyInfo, tvInquiry, tvTerms, tvLogOut, tvSignOut;

    ArrayList<ImageView> imvs;
    ArrayList<LinearLayout> lays;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);

        layAlram1 = (LinearLayout) rootView.findViewById(R.id.lay_settings_alram_1);
        layAlram2 = (LinearLayout) rootView.findViewById(R.id.lay_settings_alram_2);
        layAlram3 = (LinearLayout) rootView.findViewById(R.id.lay_settings_alram_3);

        imvAlram1 = (ImageView) rootView.findViewById(R.id.imv_settings_alram_1);
        imvAlram2 = (ImageView) rootView.findViewById(R.id.imv_settings_alram_2);
        imvAlram3 = (ImageView) rootView.findViewById(R.id.imv_settings_alram_3);

        tvNotification = (TextView) rootView.findViewById(R.id.tv_settings_notification_list);
        tvModifyInfo = (TextView) rootView.findViewById(R.id.tv_settings_modify_info);
        tvInquiry = (TextView) rootView.findViewById(R.id.tv_settings_inquiry);
        tvTerms = (TextView) rootView.findViewById(R.id.tv_settings_terms);
        tvLogOut = (TextView) rootView.findViewById(R.id.tv_settings_logout);
        tvSignOut = (TextView) rootView.findViewById(R.id.tv_settings_sign_out);

        lays = new ArrayList<>();
        lays.add(layAlram1);
        lays.add(layAlram2);
        lays.add(layAlram3);

        imvs = new ArrayList<>();
        imvs.add(imvAlram1);
        imvs.add(imvAlram2);
        imvs.add(imvAlram3);

        imvs.get(ConnectionController.sp.getInt("alramType", 0)).setSelected(true);

        for (int i = 0; i < imvs.size(); i++) {
            final int finalI = i;
            lays.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    initCheck(finalI);
                }
            });
        }

        tvNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NotificationActivity.class));
                getActivity().overridePendingTransition(0, 0);
            }
        });

        tvModifyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ModifyInfoActivity.class));
                getActivity().overridePendingTransition(0, 0);
            }
        });

        tvInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), InquiryListActivity.class));
                getActivity().overridePendingTransition(0, 0);
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TermsActivity.class));
                getActivity().overridePendingTransition(0, 0);
            }
        });

        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), LogoutPopupActivity.class), SIGNOUT_RETURN);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        tvSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), SignOutPoopupActivity.class), SIGNOUT_RETURN);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        ConnectionController.setGlobalFont(rootView);

        return rootView;

    }

    private void initCheck(int input) {
        for (int i = 0; i < imvs.size(); i++) {
            if (i == input) {
                imvs.get(i).setSelected(true);
                ConnectionController.se.putInt("alramType", i);
                ConnectionController.se.commit();
            } else {
                imvs.get(i).setSelected(false);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGOUT_RETURN:
                if (requestCode == getActivity().RESULT_OK) {
                    Toast.makeText(getActivity().getApplicationContext(), "로그아웃되었습니다.", Toast.LENGTH_SHORT).show();
                    ConnectionController.se.clear();
                    ConnectionController.se.commit();

                    getActivity().startActivity(new Intent(getActivity(), InitActivity.class));
                    getActivity().finish();
                }
                break;
            case SIGNOUT_RETURN:
                if (requestCode == getActivity().RESULT_OK) {
                    //TODO 탈퇴 API 연동
                    HashMap<String, String> params = new HashMap<>();
                    params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");

                    new APITaskNew(getActivity(), params, ConnectionController.API_SIGN_OUT, signOutResult).execute();
                }
                break;
        }
    }

    private StringTransMethod signOutResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getString("result").equals("0")) {
                    Toast.makeText(getActivity().getApplicationContext(), "탈퇴처리 되었습니다.", Toast.LENGTH_SHORT).show();
                    ConnectionController.se.clear();
                    ConnectionController.se.commit();

                    getActivity().startActivity(new Intent(getActivity(), InitActivity.class));
                    getActivity().finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
