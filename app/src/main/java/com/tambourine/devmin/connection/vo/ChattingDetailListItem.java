package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin-office on 2017-02-06.
 */

public class ChattingDetailListItem {

    private int ccId;
    private int ccSendeer;
    private int ccTarget;
    private int ccIng;
    private String ccLastStr;
    private boolean ccSenderView = false;
    private boolean ccTargetView = false;
    private int ccAntenna;
    private String ccRegDate;
    private int sex;
    private int birth;
    private int loc;

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getBirth() {
        return birth;
    }

    public void setBirth(int birth) {
        this.birth = birth;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public int getCcId() {
        return ccId;
    }

    public void setCcId(int ccId) {
        this.ccId = ccId;
    }

    public int getCcSendeer() {
        return ccSendeer;
    }

    public void setCcSendeer(int ccSendeer) {
        this.ccSendeer = ccSendeer;
    }

    public int getCcTarget() {
        return ccTarget;
    }

    public void setCcTarget(int ccTarget) {
        this.ccTarget = ccTarget;
    }

    public int getCcIng() {
        return ccIng;
    }

    public void setCcIng(int ccIng) {
        this.ccIng = ccIng;
    }

    public String getCcLastStr() {
        return ccLastStr;
    }

    public void setCcLastStr(String ccLastStr) {
        this.ccLastStr = ccLastStr;
    }

    public boolean isCcSenderView() {
        return ccSenderView;
    }

    public void setCcSenderView(boolean ccSenderView) {
        this.ccSenderView = ccSenderView;
    }

    public boolean isCcTargetView() {
        return ccTargetView;
    }

    public void setCcTargetView(boolean ccTargetView) {
        this.ccTargetView = ccTargetView;
    }

    public int getCcAntenna() {
        return ccAntenna;
    }

    public void setCcAntenna(int ccAntenna) {
        this.ccAntenna = ccAntenna;
    }

    public String getCcRegDate() {
        return ccRegDate;
    }

    public void setCcRegDate(String ccRegDate) {
        this.ccRegDate = ccRegDate;
    }
}
