package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.MinCompatActivity;

public class LoveTendencyTestIntroActivity extends MinCompatActivity {

    LinearLayout layStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love_tendenct_test_intro);

        layStart = (LinearLayout) findViewById(R.id.lay_ltti_start);

        layStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoveTendencyTestIntroActivity.this, LoveTendencyTestActivity.class));
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

}
