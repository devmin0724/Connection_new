package com.tambourine.devmin.connection.ui.popup;

import android.os.Bundle;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.MinActivity;

public class BuyCompletePopupActivity extends MinActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_complete_popup);
    }
}
