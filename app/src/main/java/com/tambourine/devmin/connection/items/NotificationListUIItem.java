package com.tambourine.devmin.connection.items;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.ui.AntennaListActivity;
import com.tambourine.devmin.connection.ui.ConnectionDetailActivity;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.vo.NotificationListItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-05-27.
 */
public class NotificationListUIItem extends AbstractItem<NotificationListUIItem, NotificationListUIItem.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public NotificationListItem item;

    public NotificationListUIItem withItem(NotificationListItem item) {
        this.item = item;
        return this;
    }

    @Override
    public int getType() {
        return R.id.notification_list_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_notification_list;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();

        switch (item.getnType()) {
            case 50001:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowlove);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment1));
                break;
            case 50002:
                holder.imvIcon.setImageResource(R.drawable.icon_lovelist_whitelove);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment2));
                break;
            case 50003:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_pinklove);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment3));
                break;
            case 50004:
                holder.imvIcon.setImageResource(R.drawable.icon_slidemenu_cash);
                holder.tvMent.setText(item.getnPoint() + con.getResources().getString(R.string.row_notification_list_ment4_2));
                break;
            case 50005:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_whitespeaker);
                holder.tvMent.setText(item.getnStr());
                break;
            case 50006:
                holder.imvIcon.setImageResource(R.drawable.icon_notice_yellowspeaker);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment4_1) + item.getnPoint() + con.getResources().getString(R.string.row_notification_list_ment4_2));
                break;
            case 50007:
                holder.imvIcon.setImageResource(R.drawable.icon_chat_yellow);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment5));
                break;
            case 50008:
                holder.imvIcon.setImageResource(R.drawable.icon_dete_chat);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment6));
                break;
            case 50009:
                holder.imvIcon.setImageResource(R.drawable.icon_chat_pink);
                holder.tvMent.setText(con.getResources().getString(R.string.row_notification_list_ment7));
                break;
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(con, ConnectionDetailActivity.class);
                Intent i2 = new Intent(con, AntennaListActivity.class);
                switch (item.getnType()) {
                    case 50001:
                        i.putExtra("id", item.getDclId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                        break;
                    case 50002:
                        i.putExtra("id", item.getDclId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                        break;
                    case 50003:
                        i.putExtra("id", item.getDclId());
                        con.startActivity(i);
                        ((Activity) con).overridePendingTransition(0, 0);
                        break;
                    case 50004:
                        con.startActivity(i2);
                        ((Activity) con).overridePendingTransition(0, 0);
                        break;
                    case 50005:
                        //TODO 공지사항
                        break;
                    case 50006:
                        //TODO 이벤트
                        break;
                }
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        @BindView(R.id.imv_row_notification_list_icon)
        protected ImageView imvIcon;
        @BindView(R.id.tv_row_notification_list_ment)
        protected TextView tvMent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
            ConnectionController.setGlobalFont(itemView);
        }
    }

}
