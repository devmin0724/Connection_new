package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoveTendencyTestActivity extends MinCompatActivity {

    LinearLayout btnYes, btnNo;
    TextView tvQ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love_tendency_test);

        btnYes = (LinearLayout) findViewById(R.id.lay_ltt_yes);
        btnNo = (LinearLayout) findViewById(R.id.lay_ltt_no);
        tvQ = (TextView) findViewById(R.id.tv_ltt_question);

        selectAnswer(0, 0);
    }

    private void questionSetting(final int qNum, final int yr, final int nr, String question) {
        switch (qNum) {
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
                Intent i = new Intent(LoveTendencyTestActivity.this, LoveTendencyTestResultActivity.class);
                i.putExtra("result", qNum);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
                break;
            default:

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectAnswer(qNum, yr);
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectAnswer(qNum, nr);
                    }
                });

                tvQ.setText(question);

                break;
        }
    }

    private void selectAnswer(int qNum, int r) {
        HashMap<String, String> params = new HashMap<>();
        params.put("question", qNum + "");
        params.put("answer", r + "");
        params.put("id", ConnectionController.sp.getString("mId", ""));

        new APITaskNew(LoveTendencyTestActivity.this, params, ConnectionController.API_LOVE_TEST, selectResult).execute();
    }

    private StringTransMethod selectResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                questionSetting(Integer.parseInt(jd.getString("lttQuestionNumber")), Integer.parseInt(jd.getString("lttYes")), Integer.parseInt(jd.getString("lttNo")), jd.getString("lttQuestion"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
