package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.NotificationListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.NotificationListItem;
import com.tambourine.devmin.connection.widget.RecyclerViewEmptySupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends MinCompatActivity {

    @BindView(R.id.rv_notification)
    RecyclerViewEmptySupport rv;
    FastItemAdapter<NotificationListUIItem> adapter;

    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        adapter = new FastItemAdapter<>();

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setEmptyView(findViewById(R.id.tv_notification_empty_content));
        rv.setAdapter(adapter);

        getData();
    }

    private void getData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", ConnectionController.sp.getInt("mIdx", 1) + "");
        params.put("page", page + "");

        new APITaskNew(this, params, ConnectionController.API_NOTIFICATION_LIST, notificationListResult).execute();
    }

    private StringTransMethod notificationListResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    NotificationListItem item = new NotificationListItem();

                    item.setnId(Integer.parseInt(jd.getString("nId")));
                    item.setmId(Integer.parseInt(jd.getString("mId")));
                    item.setnType(Integer.parseInt(jd.getString("nType")));
                    item.setnPoint(Integer.parseInt(jd.getString("nPoint")));
                    item.setnSender(Integer.parseInt(jd.getString("nSender")));
                    item.setnTarget(Integer.parseInt(jd.getString("nTarget")));
                    item.setnStr(jd.getString("nStr"));
                    item.setDclId(Integer.parseInt(jd.getString("dclId")));
                    item.setnIsView(Integer.parseInt(jd.getString("nIsView")) == 0 ? false : true);
                    item.setnRegDate(jd.getString("nRegDate"));

                    adapter.add(new NotificationListUIItem().withItem(item));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                getData();
                            }
                        }
                    }
                });
            }

            adapter.notifyDataSetChanged();

        }
    };

//    private void loadData() {
//        for (int i = 0; i < 15; i++) {
//            NotificationListItem item = new NotificationListItem();
//            item.setCount(i + 3);
//            item.setId(i + "");
//            item.setStr("");
//            item.setType(i % 6);
//            item.setStr(i % 6 == 4 ? ments[0] : ments[1]);
//
//            adapter.add(new NotificationListUIItem().withItem(item));
//        }
//
//        adapter.notifyDataSetChanged();
//    }
}
