package com.tambourine.devmin.connection.ui.popup;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.MinActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogoutPopupActivity extends MinActivity {

    @BindView(R.id.lay_logout_popup_yes)
    LinearLayout layYes;
    @BindView(R.id.lay_logout_popup_no)
    LinearLayout layNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout_popup);
        ButterKnife.bind(this);

        layYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

        layNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
