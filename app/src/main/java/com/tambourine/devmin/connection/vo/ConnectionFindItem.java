package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-11-30.
 */

public class ConnectionFindItem {

    public int sex = 0; //0:여성 1:남성
    public int age = 0;
    public int location = 0;
    public String id = "";
    public boolean isMet = false;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isMet() {
        return isMet;
    }

    public void setMet(boolean met) {
        isMet = met;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
