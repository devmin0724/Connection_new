package com.tambourine.devmin.connection.ui;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.DateListUIItem;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.DateAdBR;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.DateListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DateSelectActivity extends MinCompatActivity {

    TextView tvType;
    RecyclerView rv;

    private String id = "";
    private int type = 0, page = 1;
    private FastItemAdapter<DateListUIItem> adapter;
    private DateAdBR mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_select);

        tvType = (TextView) findViewById(R.id.tv_date_select_type);
        rv = (RecyclerView) findViewById(R.id.rv_date_select);

        try {
            id = getIntent().getExtras().getString("id");
            type = getIntent().getExtras().getInt("type");
        } catch (NullPointerException e) {
            finish();
            overridePendingTransition(0, 0);
        }

        switch (type) {
            case 0:
                tvType.setText("실시간 데이트");
                break;
            case 1:
                tvType.setText("답변형 데이트");
                break;
        }

        rv.setLayoutManager(new LinearLayoutManager(this));

        adapter = new FastItemAdapter<>();

        rv.setAdapter(adapter);

        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("com.tambourine.devmin.connection.AD_COMPLETE");

        //동적 리시버 구현
        mReceiver = new DateAdBR() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                Intent i = new Intent(ConnectionDetailActivity.this, ConnectionDetailPopupActivity.class);
//                i.putExtra("id", id);
//                startActivity(i);
//                overridePendingTransition(0, 0);
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sender", ConnectionController.sp.getInt("mIdx", 1) + "");
                params.put("target", ConnectionController.tempTargetId);
                params.put("dlId", ConnectionController.tempDlId);
                params.put("ad", "1");

                new APITaskNew(DateSelectActivity.this, params, ConnectionController.API_ASKING_FOR_DATE, afdResult).execute();
            }

        };


        loadData();

    }

    private StringTransMethod afdResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getString("result").equals("0")) {
                    finish();
                } else if (jd.getString("result").equals("2")) {
                    ConnectionController.getToast(getApplicationContext(), "안테나가 부족합니다. 광고보고 데이트하기 혹은 구매샵에서 안테나를 구입해주세요.");
                } else {
                    ConnectionController.getToast(getApplicationContext(), getResources().getString(R.string.warning_api_error));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", type + "");
        params.put("page", page + "");

        new APITaskNew(DateSelectActivity.this, params, ConnectionController.API_DATE_LIST, dlResult).execute();
    }

    private StringTransMethod dlResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);

                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    DateListItem item = new DateListItem();
                    item.setTargetId(id);
                    item.setDlId(jd.getString("dlId"));
                    item.setDlImgName(jd.getString("dlImgName"));
                    item.setDlPrice(jd.getString("dlPrice"));
                    item.setDlQuestionCount(jd.getString("dlQuestionCount"));
                    item.setDlRegDate(jd.getString("dlRegDate"));
                    item.setDlTitle(jd.getString("dlTitle"));
                    item.setDlType(jd.getString("dlType"));
                    item.setDlWriter(jd.getString("dlWriter"));

                    adapter.add(new DateListUIItem().withItem(item));
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };

}
