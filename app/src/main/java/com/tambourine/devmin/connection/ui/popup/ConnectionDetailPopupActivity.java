package com.tambourine.devmin.connection.ui.popup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.items.ConnectionDetailPopupListUIItem;
import com.tambourine.devmin.connection.ui.ConnectionDetailActivity;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.vo.ConnectionDetailPopupItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConnectionDetailPopupActivity extends MinActivity {

    @BindView(R.id.rv_connection_detail_popup)
    RecyclerView rv;
    @BindView(R.id.lay_connection_detail_popup)
    LinearLayout lay;
    @BindView(R.id.lay_connection_detail_popup_chatting)
    LinearLayout layChatting;
    @BindView(R.id.lay_connection_detail_popup_one_more)
    LinearLayout layOneMore;
    FastItemAdapter<ConnectionDetailPopupListUIItem> adapter;

    int page = 1;
    String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_detail_popup);
        ButterKnife.bind(this);

        try {
            id = getIntent().getExtras().getString("id");
        } catch (NullPointerException e) {
            finish();
        }

        rv.setLayoutManager(new LinearLayoutManager(this));

        adapter = new FastItemAdapter<>();

        rv.setAdapter(adapter);

        lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        layOneMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConnectionDetailPopupActivity.this, DateTypeSelectPopupActivity.class);
                i.putExtra("id", id);
                startActivity(i);
                finish();
            }
        });

        layChatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 채팅신청
            }
        });

        loadData();
    }

    private void loadData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        params.put("page", page + "");

        new APITaskNew(ConnectionDetailPopupActivity.this, params, ConnectionController.API_DATE_RESULT, drResult).execute();

    }

    private StringTransMethod drResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONArray jarr = new JSONArray(result);
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject jd = jarr.getJSONObject(i);
                    ConnectionDetailPopupItem item = new ConnectionDetailPopupItem();
                    item.setId(jd.getString("dcdlId"));
                    item.setQ(jd.getString("q"));
                    item.setSex(jd.getString("dcdlSex").equals("0") ? false : true);
                    item.setAnswer(jd.getString("a"));

                    adapter.add(new ConnectionDetailPopupListUIItem().withItem(item));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(10) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (currentPage > page) {
                            if (page != currentPage) {

                                page = currentPage;
                                loadData();
                            }
                        }
                    }


                });
            }
        }
    };
}
