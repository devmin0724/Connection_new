package com.tambourine.devmin.connection.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermsActivity extends MinCompatActivity {

    @BindView(R.id.lay_terms_1)
    LinearLayout lay1;
    @BindView(R.id.lay_terms_2)
    LinearLayout lay2;
    @BindView(R.id.lay_terms_3)
    LinearLayout lay3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);

        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(TermsActivity.this)
                        .toolbarColor(getResources().getColor(R.color.accent))
                        .showProgressBar(true)
                        .titleDefault("접속")
                        .showMenuCopyLink(true)
                        .showMenuRefresh(true)
                        .backPressToClose(true)
                        .webViewSupportZoom(true)
                        .show(getResources().getString(R.string.API_ROOT) + getResources().getString(R.string.URL_TERMS_1));
            }
        });
        lay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(TermsActivity.this)
                        .toolbarColor(getResources().getColor(R.color.accent))
                        .showProgressBar(true)
                        .titleDefault("접속")
                        .showMenuCopyLink(true)
                        .showMenuRefresh(true)
                        .backPressToClose(true)
                        .webViewSupportZoom(true)
                        .show(getResources().getString(R.string.API_ROOT) + getResources().getString(R.string.URL_TERMS_2));
            }
        });
        lay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(TermsActivity.this)
                        .toolbarColor(getResources().getColor(R.color.accent))
                        .showProgressBar(true)
                        .titleDefault("접속")
                        .showMenuCopyLink(true)
                        .showMenuRefresh(true)
                        .backPressToClose(true)
                        .webViewSupportZoom(true)
                        .show(getResources().getString(R.string.API_ROOT) + getResources().getString(R.string.URL_TERMS_3));
            }
        });
    }
}
