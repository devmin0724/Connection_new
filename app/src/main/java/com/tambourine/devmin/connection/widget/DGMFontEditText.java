package com.tambourine.devmin.connection.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by devmin on 2016-11-16. 1661-2754
 */

public class DGMFontEditText extends EditText {
    public DGMFontEditText(Context context) {
        super(context);
        setType(context);
    }

    public DGMFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public DGMFontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setType(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DGMFontEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setType(context);
    }

    private void setType(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "DungGeunMo.otf"));
    }
}
