package com.tambourine.devmin.connection.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinActivity;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;
import com.tambourine.devmin.connection.widget.TypeWriterTextView;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.ads.webview.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

public class InitActivity extends MinCompatActivity {


    private final int MY_PERMISSION_REQUEST_STORAGE = 100;

    private AlertDialog.Builder adb;
    AlertDialog ad;

    TypeWriterTextView tv1, tv2, tvLogo;

    int navi = 0;

    String ment1[] = {
            "ㅈ", "저", "접", "젒", "접소", "접속"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);

        tv1 = (TypeWriterTextView) findViewById(R.id.tv_init);
        tv2 = (TypeWriterTextView) findViewById(R.id.tv_init2);
        tvLogo = (TypeWriterTextView) findViewById(R.id.tv_init_logo);
        getAppKeyHash();
        adb = new AlertDialog.Builder(this);

        adb.setTitle("접속 알림");

        adb.setMessage("현재 어플이 최신버전이 아니어서 실행할 수 없습니다. 확인을 누르시면 Google PlayStore로 이동합니다.")
                .setCancelable(false)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO 마켓이동
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(Uri
                                .parse(getResources().getString(R.string.market_link)));
                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            versionCheck();
        }

    }

    private void versionCheck() {
        HashMap<String, String> params = new HashMap<>();
        new APITaskNew(this, params, ConnectionController.API_VERSION_CHECK, versionResult).execute();
    }

    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash key", something);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("name not found", e.toString());
        }
    }

    private void startInit() {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (navi >= 6) {
                    tvLogo.type("T").pause()
                            .type("B").pause()
                            .type("R").pause().run(new Runnable() {
                        @Override
                        public void run() {
//                            Log.e("gcm", FirebaseInstanceId.getInstance().getToken());
//                            Log.e("gcm", ConnectionController.PUSH_TOKEN);
                            if (ConnectionController.sp.getBoolean("isLogin", false)) {
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("id", ConnectionController.sp.getString("mId", ""));
                                params.put("pw", ConnectionController.sp.getString("mPw", ""));
                                params.put("gcm", ConnectionController.PUSH_TOKEN);
                                params.put("type", ConnectionController.sp.getInt("mJoinType", 10001) + "");

                                new APITaskNew(InitActivity.this, params, ConnectionController.API_LOGIN, loginResult).execute();
                            } else {
                                startActivity(new Intent(InitActivity.this, LoginActivity.class));
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                                finish();
                            }
                        }
                    });
                } else {
                    tv1.setText(ment1[navi]);
                    navi++;
                }
            }
        };

        handler.sendEmptyMessageDelayed(0, 500);
        handler.sendEmptyMessageDelayed(500, 1000);
        handler.sendEmptyMessageDelayed(1000, 1500);
        handler.sendEmptyMessageDelayed(1500, 2000);
        handler.sendEmptyMessageDelayed(2000, 2500);
        handler.sendEmptyMessageDelayed(2500, 3000);
        handler.sendEmptyMessageDelayed(3000, 3500);
    }

    private StringTransMethod loginResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                ConnectionController.se.putBoolean("isLogin", true);
                ConnectionController.se.putInt("mIdx", Integer.parseInt(jd.getString("mIdx")));
                ConnectionController.se.putString("mId", jd.getString("mId"));
                ConnectionController.se.putString("mName", jd.getString("mName"));
                ConnectionController.se.putInt("mLoc", Integer.parseInt(jd.getString("mLoc")));
                ConnectionController.se.putInt("mBirth", Integer.parseInt(jd.getString("mBirth")));
                ConnectionController.se.putInt("mSex", Integer.parseInt(jd.getString("mSex")));
                ConnectionController.se.putBoolean("mConfirm", Integer.parseInt(jd.getString("mConfirm")) == 0 ? false : true);
                ConnectionController.se.putBoolean("mBlock", Integer.parseInt(jd.getString("mBlock")) == 0 ? false : true);
                ConnectionController.se.putString("mBlockDate", jd.getString("mBlockDate"));
                ConnectionController.se.putString("mBlockReason", jd.getString("mBlockReason"));
                ConnectionController.se.putInt("mAntenna", Integer.parseInt(jd.getString("mAntenna")));
                ConnectionController.se.putInt("mJoinType", Integer.parseInt(jd.getString("mJoinType")));
                ConnectionController.se.putString("mLoveType", jd.getString("mLoveType"));
                ConnectionController.se.putString("mRegDate", jd.getString("mRegDate"));
                ConnectionController.se.commit();

                startActivity(new Intent(InitActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.fade, R.anim.hold);
                finish();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                    || shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(InitActivity.this, "핸드폰 저장공간 읽기/쓰기", Toast.LENGTH_SHORT).show();
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_STORAGE);
        } else {
            versionCheck();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        || grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    versionCheck();
                } else {
                    Toast.makeText(getApplicationContext(), "permission 승인 필요", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public String getVersionName() {

        try {
            PackageInfo packageInfo = getApplicationContext()
                    .getPackageManager().getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
            Log.e("vername", packageInfo.versionName);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
//            Log.i(TAG, "NameNotFoundException => " + e.toString());
            return "";
        }

    }

    private StringTransMethod versionResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject j = new JSONObject(result);

                String[] versions = j.getString("vVersion").split("/");

                for (int i = 0; i < versions.length; i++) {
                    Log.e("versions", versions[i]);
                    if (versions[i].equals(getVersionName())) {

                        startInit();
                        return;
                    }
                }

                ad = adb.create();
                ad.show();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
