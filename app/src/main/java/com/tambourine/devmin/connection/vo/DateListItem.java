package com.tambourine.devmin.connection.vo;

/**
 * Created by devmin on 2016-12-29.
 */

public class DateListItem {
    String targetId = "";
    String dlId = "";
    String dlImgName = "";
    String dlQuestionCount = "";
    String dlType = "";
    String dlTitle = "";
    String dlPrice = "";
    String dlWriter = "";
    String dlRegDate = "";

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getDlId() {
        return dlId;
    }

    public void setDlId(String dlId) {
        this.dlId = dlId;
    }

    public String getDlImgName() {
        return dlImgName;
    }

    public void setDlImgName(String dlImgName) {
        this.dlImgName = dlImgName;
    }

    public String getDlQuestionCount() {
        return dlQuestionCount;
    }

    public void setDlQuestionCount(String dlQuestionCount) {
        this.dlQuestionCount = dlQuestionCount;
    }

    public String getDlType() {
        return dlType;
    }

    public void setDlType(String dlType) {
        this.dlType = dlType;
    }

    public String getDlTitle() {
        return dlTitle;
    }

    public void setDlTitle(String dlTitle) {
        this.dlTitle = dlTitle;
    }

    public String getDlPrice() {
        return dlPrice;
    }

    public void setDlPrice(String dlPrice) {
        this.dlPrice = dlPrice;
    }

    public String getDlWriter() {
        return dlWriter;
    }

    public void setDlWriter(String dlWriter) {
        this.dlWriter = dlWriter;
    }

    public String getDlRegDate() {
        return dlRegDate;
    }

    public void setDlRegDate(String dlRegDate) {
        this.dlRegDate = dlRegDate;
    }
}
