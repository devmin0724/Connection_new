package com.tambourine.devmin.connection.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tambourine.devmin.connection.R;
import com.tambourine.devmin.connection.utils.APITaskNew;
import com.tambourine.devmin.connection.utils.ConnectionController;
import com.tambourine.devmin.connection.utils.MinCompatActivity;
import com.tambourine.devmin.connection.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FindPassActivity extends MinCompatActivity {

    EditText edtEmail;
    LinearLayout layFindPass, layInput;
    ArrayList<String> passKeys = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass);

        edtEmail = (EditText) findViewById(R.id.edt_find_pass);
        layFindPass = (LinearLayout) findViewById(R.id.lay_find_pass);
        layInput = (LinearLayout) findViewById(R.id.lay_find_pass_input);

        layFindPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkEmail();
            }
        });

        edtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    checkEmail();
                }
                return false;
            }
        });

    }

    private void checkEmail() {

        if (edtEmail.getText().toString().length() > 0) {
            HashMap<String, String> params = new HashMap<>();
            params.put("email", edtEmail.getText().toString());

            new APITaskNew(FindPassActivity.this, params, ConnectionController.API_EMAIL_CHECK, emailCheckResult).execute();
        } else {
            layInput.startAnimation(ConnectionController.shake);
        }

    }

    private StringTransMethod emailCheckResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getString("result").equals("0")) {
                    layInput.startAnimation(ConnectionController.shake);
                    Toast.makeText(getApplicationContext(), "가입된 아이디가 없습니다. 회원가입을 이용해주세요.", Toast.LENGTH_SHORT).show();
                } else if (jd.getString("result").equals("1")) {
                    String tempPasskey = getResources().getString(R.string.pass_key);

                    for (int i = 0; i < tempPasskey.length(); i++) {
                        passKeys.add(tempPasskey.substring(i, i + 1));
                    }

                    String tempPass = "";
                    for (int i = 0; i < 6; i++) {
                        tempPass += passKeys.get((int) (Math.random() * passKeys.size()));
                    }

                    HashMap<String, String> params = new HashMap<>();
                    params.put("id", edtEmail.getText().toString());
                    params.put("pw", tempPass);

                    new APITaskNew(FindPassActivity.this, params, ConnectionController.API_FIND_PW, findPWResult).execute();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_api_error), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private StringTransMethod findPWResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getString("result").equals("0")) {
                    Intent i = new Intent();
                    i.putExtra("email", edtEmail.getText().toString());
                    setResult(RESULT_OK, i);
                    overridePendingTransition(0, 0);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_api_error), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
